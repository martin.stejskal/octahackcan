# Wiring
 * You need *USB to UART converter* with **RX, TX and GND**. The bitrate
   is *115200* bps (8 bits, 1 stop bit, no parity) .
 * Arrows on PCB symbolize data flow direction from MCU point of view.
   
 ![service ui pinheader](service_ui_pinheader.jpg)

# Login
 * First, make sure that your UART is connected and you have terminal
   application open.
 * You will **not** be able to login once device switch to sleep mode. So watch
   out for power on LED.
 * Now power on device.
 * On terminal you should see following:

```
== Login ==
password:
```

 * If you do not see this, reset device by power on and off again. For example
   via power switch or if you're connected to the car, remove keys from
   ignition, wait 10 seconds and put keys back to ignition and move to position
   "on" (not necessary start engine).
 * Type password. Default is `octahack` and it is defined in UI service at user
   application part.

# Work with service UI
 * Once you login, system should print current help:

```
== Login ==
password: octahack
[OK]


==<| Help |>==
 Here is list of available commands:
```

 * This might change, so simply read instructions and examples in this help :)
 * If you type incorrect command or pass invalid argument, system should report
   it to you.
 * Remember: when you operate in service UI, other operations are postponed and
   device will **not** go to sleep mode! You need to call command `exit` or
   restart device.

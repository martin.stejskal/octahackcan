# About
 * Target is to have device that can lock or unlock *Skoda Octavia mkI (tour)*
   via trunk latch switch. Plus bit more, like sniffing CAN bus or injecting
   own messages :)
   
 * Device can learn *lock* and *unlock* patterns (code lock) which provides
   at least some kind of security.
   
   ![img](doc/device_1.jpg)
   
   ![img](doc/device_2.jpg)

# HW
 * MCU: ESP32
 * PCB design - recommended to use version **02**. Version 00 was
   purely for development purposes and contains some errors. Version 01
   fix some problems, but it does not add protection resistors on CAN bus.
   So version 02 allow to do that, plus allow to choose between CAN transmitter
   (*MCP2551* vs *SN65HVD230*). I had issues with malfunctioning *MCP2551*,
   so I decided for *SN65HVD230* and so far so good.
 * While **active** (CAN active, trunk button pressed), device consume at about
   **35 mA @ 12V**.
 * In **deep sleep** mode (CAN inactive, trunk button released), it is at about
   **1.7 mA**. Mainly because of 3.3 V stabilizer (AMS1117). Other components
   consume few tens of uA. If power consumption is not acceptable for you,
   simply focus on that stabilizer.

# FW
 * Learning patterns algorithm is implemented. So you can learn device your own
   code ;)
 * CAN sends real commands. Allow to really lock and unlock car.
 * There is *service UI* interface (*J4* - UART 115200) which allow to do some
   advanced settings. It also allow to transmit or receive data over CAN bus.
   So eventually it can be used as primitive sniffer and injector too.

# Wiring


 * You will need to get *+12 V, GND, CAN H, CAN L* and *trunk latch switch*
   wires from your car first. Locate comfort unit under dashboard. 
 * Unplug battery first.

 ![comfort unit location](doc/comfort_unit_location.jpg)

 * Unplug battery. Connect it back only when it is last resort, do test and
   unplug again.
 * Before doing any further changes, remove comfort unit and make sure that you
   have `1C0 959 799 A`. **If you have anything else, CAN codes will most**
   **probably not work! Also if you have different numbers of connectors**
   **here, it will also not work!** Keep this in mind. However it is possible
   to change/add new codes, although it is quite hard to figure it out.
 * If everything match, focus on "middle" connector with 23 pins. Take this
   female connector from bottom and make sure that following pinout matches:

```
+-----------------+
| G o G o o H L o |
|   o o G o o o x |
| + o o o G o o o |
+-----------------+
```

 * Legend:
   * G - GND
   * "+" - 12 V (fused battery)
   * L - CAN LOW
   * H - CAN HIGH
   * x - trunk switch. If you connect it with GND via 1 kOhm, it should open
     trunk latch.
 * If you are not sure about CAN L pin, just plug in ignition key and turn it
   into "on" state, but do not start engine. Voltage should drop to less than
   6 V. If you remove ignition key completely and lock car, after few seconds
   voltage should raise back to 12 V (there is small drop, so do not panic that
   voltage does not exactly match with battery voltage).
 * CAN LOW and HIGH can be tested by oscilloscope. It is differential signal at
   around 2.5 V (0~5 V range). Just insert ignition key, turn it to position
   "on", but do not start engine. Control unit should produce enough bus
   traffic, so it should be easy to capture it.
 * If you have confirmed all the wiring above and control unit numbers match,
   you have some chances :)

# Build HW

 * You will need ESP32, some USB to UART converter, few NPN transistors, few
   P channel MOS-FETs, stabilizer (3.3 V, 5 V), CAN transceiver  and some
   garbage around (diodes, capacitors and resistors).
 * Simply take a look to [BOM](hw/export/OCTA-HACK-01_BOM.csv).
 * Then assembly according to [exported PDF](hw/export/OCTA-HACK-01_pcb.pdf),
   page 19 and 20.

# Flash device

 * You will need some USB to UART converter. I can recommend same as below:

 ![img](doc/usb_uart_converter.jpg)

 * If you want to build from source, you probably know what to do. You need to
   have [ESP IDF](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/)
   and setup your environment.

 * In case you just downloaded binaries (from 
   [pipeline](https://gitlab.com/martin.stejskal/octahackcan/-/pipelines)
    -> make sure that *master* is in the filename), you have 2 options:

   * Use
     [GUI tool for Windows](https://www.espressif.com/en/support/download/other-tools),
     where you have to define binary and it's address, flash size etc. Simply
     take a look to command below and you will figure out file name and address
     offset as well as other parameters ;)
   * In case of Linux, simply use IDF `esptool.py` (part of mentioned ESP IDF)
     and basically copy/paste following command:

```bash
esptool.py -p /dev/ttyUSB0 \
    -b 460800 --before default_reset --after hard_reset --chip esp32 \
    write_flash --flash_mode dio --flash_size detect --flash_freq 40m \
    0x1000 build/bootloader/bootloader.bin \
    0x8000 build/partition_table/partition-table.bin \
    0x10000 build/OctaHackCan.bin
```

   * In case of `esptool.py`, **pay attention to port**. You do not need to
     specify port (`-p /dev/ttyUSB0`) if you have only 1 USB to UART converter
     connected to your computer.  If you're using more converters, make sure
     you defined correct one. On Windows naming bit differs, so you should
     input something like `COM1`. 

 * Then connect USB to UART converter to *Programming* pinheader.

  ![programming header](doc/programming_pinheader.jpg)

 *  Note that *TX* direction means "from board" and *RX* means "into board".
    So you might need to swap those when connecting to programmer. The *RTS*
    and *DTR* signals are connected directly. No swapping please.

 * If you do not have *RTS* and *DTR* signals on your converter, you will need
   to set ESP into boot mode manually. Disconnect power supply from device.
   Then set by 3.3 V logic *RTS* to high and *DTR* to low. When power on
   device. This should initiate boot process.

 * Use IDF script to build and flash device: `idf.py build flash`. If you're
   using more than 1 USB to UART converter, you might need to define port (add
   parameter `-p /dev/ttyUSB3` for example).

 * If you have any troubles, you can always add another UART converter (115200)
   to *TXD0* pin to see what is going on.

# Setup device

 * On PCB are 2 LED. The RGB signalize status and can help you to figure out
   what is device trying to "say". Second one is simple LED which indicate
   whether device is in deep sleep mode (off) or not (on). So if something does
   not work, make sure that *power on LED* is actually on.
 * In order to keep device awake, CAN bus have to be active. It is recommended
   to have ignition key in "on" position, but do not start engine.
 * After flashing and reboot, device should do annoying beep sound and RGB LED
   will change color to yellow. This signalize that device need to be
   configured.
 * Once you will set all codes, it will not be possible to change them. It
   would be pointless to have codes set if you could change them later, right?
   Only way out is factory reset described later.

# Set unlock code

 * Go to trunk latch and **press trunk switch for 5 seconds**. Then release it.
 * Buzzer will signalize it by **3x short beeps**. LED will change color to
   **turquoise**.
 * Now you have to input your code. If you will release button for more than
   10 seconds, it is considered that sequence is complete.
 * When sequence is saved, LED will change color back to yellow (you need to
   set lock code) or it will be off (all codes are set).

## Set lock code

 * Go to trunk latch and **press trunk switch for 8 seconds**. Then release it.
 * Buzzer will signalize it by **3x long beeps**. LED will change color to
   **purple**.
 * Now you have to input your code. If you will release button for more than
   10 seconds, it is considered that sequence is complete.
 * When sequence is saved, LED will change color back to yellow (you need to
   set unlock code) or it will be off (all codes are set).

# Testing

 * Remember that system will go to sleep mode if CAN is not active for
   10 seconds.
 * Pressing trunk button or making CAN active wake up processor.
 * Also button code decoder will reset after 10 seconds if trunk latch button
   is not pressed. So if your code will be input incorrectly, you need to wait
   10 seconds before you can re-try.
 * **Always wear your keys in the pocket!!!** If you will lock your car, you
   might have troubles to get inside if you forget sequence. Even if you are
   inside, mechanics might not allow you to get out!
 * You had been warned.

# Unlock car

 * Get out of car, and **lock car by remote**.
 * Wait 10 seconds or more and move to trunk.
 * Do 1 short "click". This will wake up device. Wait about 2 seconds.
 * Enter your code. You have at about 9 seconds to do so, otherwise device will
   set itself back to sleep mode.
 * When unlocking, device should also do beep sound.
 * If you have problems with unlocking, use remote to get in :) Below is
   explained how to reset device to factory settings, so you can start from
   scratch.

# Lock car

 * Get out of car and **double check that you have keys in your hands**.
 * Wait 10 seconds. CAN should be inactive that time. You can always check
   "power on LED". If it is on, CAN is still active.
 * Move to trunk.
 * Do 1 short "click". This will wake up device, but also open trunk. Do not
   close trunk doors yet. Wait about 2 seconds.
 * Input lock code. You have at about 9 seconds to do so, otherwise device will
   set itself back to sleep mode.
 * When locking, device should also do beep sound.
 * If you have problems with locking, try remote. If remote key is working, you
   might want to reset device to factory settings and try again.

# Factory reset

 * On device is actually also button.
 * Put ignition key to the "on" position, but do not start engine.
 * Keep button pressed for at least 5 seconds.
 * Device should set RGB LED to white color and let buzzer scream.
 * Then device is reset. You need to setup all the codes again.

# Setting sensitivity

 * Needles to to say that this may make your code too benevolent or too strict.
 * As you might noticed, there is potentiometer. But instead of volume it set
   "benevolence level". Turning it clockwise it will make system more
   benevolent.
 * Put ignition key to the "on" position, but do not start engine.
 * Keep button pressed for 2 seconds and then release it.
 * Device will set RGB to turquoise and it will do few quick beeps.
 * After 10 seconds you can give a try.
 * If you will have some struggle, you can always reset device to factory
   settings.

# Advanced
 * If you want explore project bit deeper, take a look to
   [doc/README.md](doc/README.md).

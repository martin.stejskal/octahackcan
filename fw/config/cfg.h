/**
 * @file
 * @author Martin Stejskal
 * @brief Configuration file for Anomaly meter
 *
 * Mainly here is HW configuration
 */
#ifndef __CFG_H__
#define __CFG_H__
// ===============================| Includes |================================
// Kind of generic, but Espressif use it too
#include <driver/adc.h>
#include <time.h>
// ==================================| I/O |==================================
/**
 * @brief I/O configuration
 *
 * @{
 */

/**
 * @brief Button code lock
 */
#define BTN_CODE_LOCK_PIN (33)

/**
 * @brief Control 5V regulator
 *
 * @note Need to be enabled in order to be able use CAN transceiver in case
 *       that MCP2551 CAN transceiver is used.
 *
 */
#define PWR_5V_EN_PIN (32)

/**
 * @brief ADC channel for tolerance threshold pin
 *
 * @warning It have to be ADC2 channel!
 *
 * @note ADC channel number have to be used instead of pin number.
 */
#define TOLERANCE_THRESHOLD_PIN (ADC2_CHANNEL_7)
// ==============================| CAN related |==============================
/**
 * @brief CAN related
 *
 * @{
 */
/**
 * @brief Can active detector
 *
 */
#define CAN_ACTIVE_PIN (35)

/**
 * @brief CAN TX
 */
#define CAN_TX_PIN (21)

/**
 * @brief CAN RX
 */
#define CAN_RX_PIN (22)

/**
 * @brief Enable CAN transceiver
 *
 * @note Enabling 5V power supply is required
 */
#define CAN_TRNSCVR_PWR_PIN (4)
/**
 * @}
 */

/**
 * @brief User reset button
 *
 * This button is active only when CAN is active and allow restore default
 * codes
 */
#define FACTORY_RESET_PIN (34)

/**
 * @brief Define which DAC will be used for buzzer
 *
 * DAC channel 1 is connected to pin 25, channel 2 to pin 26
 */
#define BUZZER_DAC_PIN (DAC_CHANNEL_2)

// ==============================| LED related |==============================
/**
 * @brief LED related
 *
 * @{
 */
/**
 * @brief LED signalize that device is in active state
 */
#define LED_POWER_ON_PIN (2)

/**
 * @brief RGB LED
 *
 * @{
 */

/**
 * @brief Red channel at RGB LED
 */
#define LED_R_PIN (5)

/**
 * @brief Green channel at RGB LED
 */
#define LED_G_PIN (18)

/**
 * @brief Blue channel at RGB LED
 */
#define LED_B_PIN (19)

/**
 * @brief PWM channel for red LED
 */
#define LED_R_PWM_CH (LEDC_CHANNEL_0)

/**
 * @brief PWM channel for green LED
 */
#define LED_G_PWM_CH (LEDC_CHANNEL_1)

/**
 * @brief PWM channel for blue LED
 */
#define LED_B_PWM_CH (LEDC_CHANNEL_2)

/**
 * @brief Timer for generating PWM for power LED
 */
#define LED_RGB_TIMER (LEDC_TIMER_0)
/**
 * @}
 */
/**
 * @}
 */

// ==============================| Service UI |===============================
/**
 * @brief Service UI IO
 *
 * @{
 */
/**
 * @brief Service UI baudrate
 */
#define UI_SERVICE_UART_BAUDRATE (115200)

/**
 * @brief Service UI UART interface
 */
#define UI_SERVICE_UART_INTERFACE (UART_NUM_1)

/**
 * @brief Service UI TX pin
 */
#define UI_SERVICE_TDX_PIN (GPIO_NUM_17)

/**
 * @brief Service UI RX pin
 */
#define UI_SERVICE_RXD_PIN (GPIO_NUM_16)
/**
 * @}
 */

/**
 * @}
 */

// =============================| Time related |==============================
/**
 * @brief Time related stuff
 *
 * @{
 */
/**
 * @brief Deep sleep timeout in seconds
 *
 * When there will be no activity for this period, device will be put into
 * deep sleep mode
 */
#define NO_ACTIVITY_TIMEOUT_S (10)

/**
 * @brief Factory reset button press in seconds
 *
 * When "factory reset" button is pressed for this amount of time and CAN is
 * active (car is "woken up") factory settings will be restored
 */
#define FACTORY_RESET_DURATION_S (5)

/**
 * @brief Threshold for setting new sensitivity threshold
 *
 * When "reset button" pressed for this amount of time, new threshold for
 * commands will be set according to potentiometer position.
 */
#define SET_SENSITIVITY_THRESHOLD_S (2)

/**
 * @brief Beep duration in ms when factory reset is applied
 *
 * When factory reset is applied, it should somehow inform user. Time here
 * define how long beep will be.
 */
#define FACTORY_RESET_BEEP_DURATION_MS (2000)

/**
 * @brief Define function which returns ms (32 bit value) as time reference
 */
#define GET_CLK_MS_32BIT() esp_log_timestamp()

/**
 * @}
 */

// =========================| Buzzer and LED events |=========================
/**
 * @brief Buzzer and LED events
 *
 * @{
 */

/**
 * @brief Beep when factory reset is applied
 *
 * Typically this should be maximum possible number, to keep beep sound until
 * reboot occurs.
 */
#define BEEP_FACTORY_RESET_MS (0xFFFF)

/**
 * @brief Beep when new threshold for code lock is set
 */
#define BEEP_NEW_CODE_LOCK_THRESHOLD \
  { .u16timeBeepMs = 50, .u16timeSilenceMs = 50, .u16repeatCount = 5 }

/**
 * @brief How long should signalize that new threshold was set
 */
#define LED_NEW_CODE_LOCK_THRESHOLD_MS (1000)
/**
 * @}
 */
// ============================| Default values |=============================
/**
 * @brief By default this should be off
 *
 * This allow to disable all sleep related functions in order to keep core
 * running so debugger can attach to the target.
 */
#define HW_DEBUG_ON (0)

/**
 * @brief When enabled, it shows stack usage
 */
#define SHOW_STACK_USAGE (0)
// ===========================| Structures, enums |===========================
#endif  // __CFG_H__

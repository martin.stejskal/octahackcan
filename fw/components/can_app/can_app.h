/**
 * @file
 * @author Martin Stejskal
 * @brief Application for CAN bus
 */
#ifndef __CAN_APP_H__
#define __CAN_APP_H__
// ===============================| Includes |================================
#include <esp_err.h>
#include <stdbool.h>
#include <stdint.h>

// ESP specific
#include <driver/twai.h>  // Aka CAN driver
// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
/**
 * @brief Payload structure
 */
typedef struct {
  /**
   * @brief Identifier
   *
   * Typically 11 or 29 bit long
   */
  uint32_t u32_id;

  /**
   * @brief Data payload array
   *
   * Typically maximum is 8 Bytes
   */
  uint8_t u8_data[TWAI_FRAME_MAX_DLC];

  /**
   * @brief Length of data in Bytes
   */
  uint8_t u8_data_len;

  /**
   * @brief Delay after required operation in ms
   *
   * When RX or TX operation finished, there is this optional delay in ms.
   * If value is zero, do delay is applied.
   */
  uint16_t u16_delay_after_ms;

  /**
   * @brief Define timeout in ms for this transaction
   *
   * Value 0 means maximum possible timeout.
   */
  uint32_t u32_operation_timeout_ms;
} ts_can_app_payload;

/**
 * @brief Callback which allow to enable/disable CAN transceiver
 */
typedef void (*tf_can_app_power_transciever)(bool b_enable);

/**
 * @brief Argument structure when initializing CAN application
 *
 *
 */
typedef struct {
  // Callback function which allow to enable and disable transceiver
  tf_can_app_power_transciever pf_enable_transciever_cb;
} ts_can_app_args;
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =========================| High level functions |==========================
/**
 * @brief Initialize CAN application
 *
 * @param p_args Pointer to settings. Function will make copy inside. If empty,
 *               then default settings will be used.
 * @return Error code. ESP_OK if no error
 */
esp_err_t can_app_init(const ts_can_app_args *p_args);

/**
 * @brief Send lock sequence
 * @return Error code. ESP_OK if no error
 */
esp_err_t can_app_lock(void);

/**
 * @brief Send unlock sequence
 * @return Error code. ESP_OK if no error
 */
esp_err_t can_app_unlock(void);

/**
 * @brief Deinitialize CAN application including HW
 * @return Error code. ESP_OK if no error
 */
esp_err_t can_app_denit(void);
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
/**
 * @brief Transmit set of messages
 *
 * @param pas_payloads Pointer to array of payload structures
 * @param u16_num_of_structures Number of payload structures to be processed
 * @return Error code. ESP_OK if no error
 */
esp_err_t can_app_tx(ts_can_app_payload *pas_payloads,
                     const uint16_t u16_num_of_structures);

/**
 * @brief Receive number of required messages
 *
 * For every required payload is process waiting u32operationTimeoutMs. If
 * timeout occurs, whole RX process is aborted
 *
 * @param pas_payloads Pointer to memory, where payload data will be stored
 * @param u16_num_of_structures Number of required structures
 * @return Error code. ESP_OK if no error
 */
esp_err_t can_app_rx(ts_can_app_payload *pas_payloads,
                     const uint16_t u16_num_of_structures);

#endif  // __CAN_APP_H__

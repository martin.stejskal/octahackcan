/**
 * @file
 * @author Martin Stejskal
 * @brief Configuration file for CAN application module
 */
#ifndef __CAN_APP_CFG_H__
#define __CAN_APP_CFG_H__
// ===============================| Includes |================================
// ================================| Defines |================================
/**
 * @brief Data bitrate for CAN bus. Bits per seconds
 */
#define CAN_APP_BITRATE_BPS (100000)

/**
 * @brief When sending data there should be at least some minimal delay
 *
 * Value is in ms. Recommended value is at least 10 ms. Problem can happen if
 * messages are "squeezed" to each other.
 */
#define CAN_APP_MIN_CAN_DELAY_MS (30)

#define CAN_APP_PREDEF_CODES_OPERATION_TIMEOUT_MS (2000)
// ============================| Default values |=============================

/**
 * @brief Lock sequence for Skoda Octavia mkI - comfort unit 3rd generation
 */
#define CAN_APP_LOCK_CODE_SKODA_OCT_I_COMF_UNIT_3RD_GEN                      \
  {                                                                          \
    {.u32_id = 0x291,                                                        \
     .u8_data = {0x8A, 0x00, 0x01},                                          \
     .u8_data_len = 3,                                                       \
     .u16_delay_after_ms = 500,                                              \
     .u32_operation_timeout_ms = CAN_APP_PREDEF_CODES_OPERATION_TIMEOUT_MS}, \
        {.u32_id = 0x291,                                                    \
         .u8_data = {0x8A, 0x00, 0x01},                                      \
         .u8_data_len = 3,                                                   \
         .u16_delay_after_ms = CAN_APP_MIN_CAN_DELAY_MS,                     \
         .u32_operation_timeout_ms =                                         \
             CAN_APP_PREDEF_CODES_OPERATION_TIMEOUT_MS},                     \
    {                                                                        \
      .u32_id = 0x281, .u8_data = {0x15, 0x00}, .u8_data_len = 2,            \
      .u16_delay_after_ms = CAN_APP_MIN_CAN_DELAY_MS,                        \
      .u32_operation_timeout_ms = CAN_APP_PREDEF_CODES_OPERATION_TIMEOUT_MS, \
    }                                                                        \
  }

/**
 * @brief Unlock sequence for Skoda Octavia mkI - comfort unit 3rd generation
 */
#define CAN_APP_UNLOCK_CODE_SKODA_OCT_I_COMF_UNIT_3RD_GEN                    \
  {                                                                          \
    {.u32_id = 0x281,                                                        \
     .u8_data = {0x85, 0x00},                                                \
     .u8_data_len = 2,                                                       \
     .u16_delay_after_ms = CAN_APP_MIN_CAN_DELAY_MS,                         \
     .u32_operation_timeout_ms = CAN_APP_PREDEF_CODES_OPERATION_TIMEOUT_MS}, \
        {.u32_id = 0x291,                                                    \
         .u8_data = {0x4A, 0xAA, 0x00},                                      \
         .u8_data_len = 3,                                                   \
         .u16_delay_after_ms = 500,                                          \
         .u32_operation_timeout_ms =                                         \
             CAN_APP_PREDEF_CODES_OPERATION_TIMEOUT_MS},                     \
        {.u32_id = 0x291,                                                    \
         .u8_data = {0x4A, 0xAA, 0x00},                                      \
         .u8_data_len = 3,                                                   \
         .u16_delay_after_ms = CAN_APP_MIN_CAN_DELAY_MS,                     \
         .u32_operation_timeout_ms =                                         \
             CAN_APP_PREDEF_CODES_OPERATION_TIMEOUT_MS},                     \
  }
#endif  // __CAN_APP_CFG_H__

/**
 * @file
 * @author Martin Stejskal
 * @brief Application for CAN bus
 */
// ===============================| Includes |================================
#include "can_app.h"

#include <assert.h>
#include <driver/gpio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <string.h>

#include "can_app_cfg.h"
#include "cfg.h"  // Project specific settings

// ESP specific
#include <esp_err.h>
#include <esp_log.h>
// ================================| Defines |================================
/**
 * @brief Get number of items in array
 */
#define _NUM_OF_ITEMS(array) (sizeof(array) / sizeof(array[0]))
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
typedef struct {
  const twai_timing_config_t s_time;
  const twai_filter_config_t s_filter;
  const twai_general_config_t s_general;
} ts_can_bus_cfg;

typedef enum { CAN_APP_DIR_RX, CAN_APP_DIR_TX } te_direction;

typedef struct {
  ts_can_app_args s_args;
} ts_can_app_runtime;
// ===========================| Global variables |============================
/**
 * @brief Tag for logger
 */
static const char *TAG = "CAN app";

static const ts_can_bus_cfg ms_can_bus_cfg = {
    .s_time = TWAI_TIMING_CONFIG_100KBITS(),

    // Do not filter anything. RX all data
    .s_filter = TWAI_FILTER_CONFIG_ACCEPT_ALL(),
    .s_general =
        TWAI_GENERAL_CONFIG_DEFAULT(CAN_TX_PIN, CAN_RX_PIN, TWAI_MODE_NORMAL),
};

/**
 * @brief Keeps runtime variables together
 *
 * By default, no callback is set
 */
static ts_can_app_runtime ms_can_app_runtime = {
    .s_args = {.pf_enable_transciever_cb = NULL},
};
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
/**
 * @brief Initialize CAN interface on MCU
 * @return ESP_OK if no error
 */
static esp_err_t _init_can(void);

/**
 * @brief Generic function for receiving and transmitting
 *
 * Lot of code can be shared actually, so it make sense to take advantage of it
 *
 * @param pas_payloads Pointer to source or destination data
 * @param u16_num_of_structures Number of required transmissions to transmit or
 *                           receive
 * @param e_direction Direction - RX or TX
 * @return ESP_OK if no error
 */
static esp_err_t _tx_rx_core(ts_can_app_payload *pas_payloads,
                             const uint16_t u16_num_of_structures,
                             const te_direction e_direction);

/**
 * @brief Stop TWAI (CAN) and disable transceiver
 * @param e_err_code Error code which will be passed through or updated
 * @return ESP_OK if no error
 */
static esp_err_t _exit_gracefully(esp_err_t e_err_code);

/**
 * @brief Fill structure suitable for TWAI (CAN) HAL driver
 * @param[out] ps_twai_structure Pointer to output structure
 * @param[in] ps_user_payload Input data as user payload structure
 * @param e_direction Direction - RX or TX
 * @return ESP_OK if no error
 */
static esp_err_t _fill_twai_struct(twai_message_t *ps_twai_structure,
                                   const ts_can_app_payload *ps_user_payload,
                                   const te_direction e_direction);

static void _print_can_msg(const twai_message_t *ps_msg);
// =========================| High level functions |==========================
esp_err_t can_app_init(const ts_can_app_args *p_args) {
  ESP_LOGI(TAG, "Initializing CAN application");

  // If some arguments given, copy settings
  if (p_args) {
    memcpy(&ms_can_app_runtime.s_args, p_args,
           sizeof(ms_can_app_runtime.s_args));
  }

  return _init_can();
}

esp_err_t can_app_lock(void) {
  ts_can_app_payload asCanPayloads[] =
      CAN_APP_LOCK_CODE_SKODA_OCT_I_COMF_UNIT_3RD_GEN;

  return can_app_tx(asCanPayloads, _NUM_OF_ITEMS(asCanPayloads));
}

esp_err_t can_app_unlock(void) {
  ts_can_app_payload asCanPayloads[] =
      CAN_APP_UNLOCK_CODE_SKODA_OCT_I_COMF_UNIT_3RD_GEN;

  return can_app_tx(asCanPayloads, _NUM_OF_ITEMS(asCanPayloads));
}

esp_err_t can_app_denit(void) {
  // Expecting that interface is already stopped
  esp_err_t eErrCode = twai_driver_uninstall();

  // Deinitialize GPIO
  gpio_config_t sCfg = {
      .pin_bit_mask = BIT64(CAN_TX_PIN) | BIT64(CAN_RX_PIN),
      .mode = GPIO_MODE_DISABLE,
      .pull_up_en = false,  // Need to be disabled so they will not glow
      .pull_down_en = false,
      .intr_type = GPIO_INTR_DISABLE,
  };
  gpio_config(&sCfg);

  return eErrCode;
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
esp_err_t can_app_tx(ts_can_app_payload *pas_payloads,
                     const uint16_t u16_num_of_structures) {
  return _tx_rx_core(pas_payloads, u16_num_of_structures, CAN_APP_DIR_TX);
}

esp_err_t can_app_rx(ts_can_app_payload *pas_payloads,
                     const uint16_t u16_num_of_structures) {
  return _tx_rx_core(pas_payloads, u16_num_of_structures, CAN_APP_DIR_RX);
}
// ==========================| Internal functions |===========================
static esp_err_t _init_can(void) {
  return twai_driver_install(&ms_can_bus_cfg.s_general, &ms_can_bus_cfg.s_time,
                             &ms_can_bus_cfg.s_filter);
}

static esp_err_t _tx_rx_core(ts_can_app_payload *pas_payloads,
                             const uint16_t u16_num_of_structures,
                             const te_direction e_direction) {
  // Pointer and number of structures should not be empty
  assert(pas_payloads);

  esp_err_t e_err_code;

  // If number of structures is zero return error
  if (!u16_num_of_structures) {
    return ESP_ERR_INVALID_ARG;
  }

  // The TWAI interface need to "know" few more things then payload -> need
  // to use this structure and push data there
  twai_message_t s_twai_payload;

  // If callback for power on CAN transceiver is set, call it
  if (ms_can_app_runtime.s_args.pf_enable_transciever_cb) {
    ms_can_app_runtime.s_args.pf_enable_transciever_cb(true);
  }

  e_err_code = twai_start();
  if (e_err_code) {
    ESP_LOGE(TAG, "TWAI start failed: %s", esp_err_to_name(e_err_code));
    return e_err_code;
  }

  for (uint16_t u16_transaction_idx = 0;
       u16_transaction_idx < u16_num_of_structures; u16_transaction_idx++) {
    // Fill TWAI structure & check error code
    e_err_code = _fill_twai_struct(
        &s_twai_payload, &pas_payloads[u16_transaction_idx], e_direction);
    if (e_err_code) {
      ESP_LOGE(TAG, "Filling TWAI structure failed: %s",
               esp_err_to_name(e_err_code));
      return _exit_gracefully(e_err_code);
    }

    switch (e_direction) {
      case CAN_APP_DIR_RX:
        // Try to RX data
        e_err_code =
            twai_receive(&s_twai_payload,
                         pdMS_TO_TICKS(pas_payloads->u32_operation_timeout_ms));

        if (e_err_code) {
          // Timeout is kind of expected, do not print it as error
          if (e_err_code != ESP_ERR_TIMEOUT) {
            ESP_LOGE(TAG, "TWAI RX failed: %s", esp_err_to_name(e_err_code));
          }

          return _exit_gracefully(e_err_code);
        }

        // Push data to user payload structure
        pas_payloads[u16_transaction_idx].u32_id = s_twai_payload.identifier;

        // If there are any data, copy them
        if (s_twai_payload.data_length_code) {
          memcpy(pas_payloads[u16_transaction_idx].u8_data, s_twai_payload.data,
                 s_twai_payload.data_length_code);
        }

        pas_payloads[u16_transaction_idx].u8_data_len =
            s_twai_payload.data_length_code;

        // And print received data into debug output
        _print_can_msg(&s_twai_payload);
        break;

      case CAN_APP_DIR_TX:
        // TX data
        e_err_code = twai_transmit(
            &s_twai_payload,
            pdMS_TO_TICKS(pas_payloads->u32_operation_timeout_ms));
        if (e_err_code) {
          ESP_LOGE(TAG, "TWAI TX failed: %s", esp_err_to_name(e_err_code));

          return _exit_gracefully(e_err_code);
        }

        break;

      default:
        ESP_LOGE(TAG, "Invalid direction value: %d", e_direction);
        assert(0);
        return _exit_gracefully(e_err_code);
    }

    // If there is required delay, wait
    if (pas_payloads[u16_transaction_idx].u16_delay_after_ms) {
      vTaskDelay(
          pdMS_TO_TICKS(pas_payloads[u16_transaction_idx].u16_delay_after_ms));
    }
  }

  return _exit_gracefully(e_err_code);
}

static esp_err_t _exit_gracefully(esp_err_t e_err_code) {
  twai_stop();

  // If callback is set, disable CAN transceiver
  if (ms_can_app_runtime.s_args.pf_enable_transciever_cb) {
    ms_can_app_runtime.s_args.pf_enable_transciever_cb(false);
  }

  return e_err_code;
}

static esp_err_t _fill_twai_struct(twai_message_t *ps_twai_structure,
                                   const ts_can_app_payload *ps_user_payload,
                                   const te_direction e_direction) {
  // Pointers should not be empty
  assert(ps_twai_structure);
  assert(ps_user_payload);
  assert((e_direction == CAN_APP_DIR_RX) || (e_direction == CAN_APP_DIR_TX));

  // Check input arguments
  if (e_direction == CAN_APP_DIR_TX) {
    if ((ps_user_payload->u8_data_len > TWAI_FRAME_MAX_DLC) ||
        (ps_user_payload->u8_data_len == 0)) {
      return ESP_ERR_INVALID_ARG;
    }
  } else {
    // In case of RX length is irrelevant
  }

  // Clean up target structure. No matter direction
  memset(ps_twai_structure, 0, sizeof(twai_message_t));

  // Set flags. This depends on direction. It is kind of black magic for me
  // but this settings just works, so do not fix what ain't broken
  if (e_direction == CAN_APP_DIR_RX) {
    ps_twai_structure->self = 1;

    // Other fields should stay zero
  } else {
    // TX direction - need to fill "few" things

    // If ID is longer than 11 bits, then use 29 bit long ID -> if any bit
    // above 11th is set, use 29 long ID
    if (ps_user_payload->u32_id & 0xFFF8000) {
      ps_twai_structure->extd = 1;
    }

    // Copy ID
    ps_twai_structure->identifier = ps_user_payload->u32_id;

    // Copy data
    memcpy(&ps_twai_structure->data, &ps_user_payload->u8_data,
           sizeof(ps_twai_structure));

    // Copy data length information
    ps_twai_structure->data_length_code = ps_user_payload->u8_data_len;
  }

  return ESP_OK;
}

static void _print_can_msg(const twai_message_t *ps_msg) {
  // Disable printing right here, because it slows down processing. Enable only
  // for debug purpose
  return;

  // Every Byte can be represented as 5 characters (separated by space):
  // "0xFF " -> 5 characters. Do not forget about tailing termination character
  char ac_data_ascii[5 * TWAI_FRAME_MAX_DLC + 1] = {0};

  // One Byte converted to ASCII - 5 characters + termination character
  char ac_hex_digit[5 + 1];

  for (uint8_t u8_byte_idx = 0; u8_byte_idx < ps_msg->data_length_code;
       u8_byte_idx++) {
    sprintf(ac_hex_digit, "0x%02x ", ps_msg->data[u8_byte_idx]);
    strcat(ac_data_ascii, ac_hex_digit);
  }

  ESP_LOGD(TAG, "ID 0x%02x DATA %s", ps_msg->identifier, ac_data_ascii);
}

/**
 * @file
 * @author Martin Stejskal
 * @brief Power manager which allow easily power on/off modules
 */
// ===============================| Includes |================================
#include "power_manager.h"

#include <driver/gpio.h>

#include "cfg.h"
// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================
#ifndef PWR_5V_EN_PIN
#error "You have to define pin for enabling 5V power supply"
#endif

#ifndef CAN_TRNSCVR_PWR_PIN
#error "You have to define pin for enabling CAN transceiver"
#endif
// =======================| Structures, enumerations |========================

typedef struct {
  // GPIO already initialized?
  bool b_gpio_initialized;

  // The +5V power supply enabled?
  bool b_5v_pwr_supply;

  // CAN transceiver enabled?
  bool b_can_transceiver;
} ts_pm_runtime;
// ===========================| Global variables |============================

static ts_pm_runtime ms_pm_runtime = {
    .b_gpio_initialized = false,
    .b_5v_pwr_supply = false,
    .b_can_transceiver = false,
};
// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
// ==============| Internal function prototypes: middle level |===============
// ================| Internal function prototypes: low level |================
/**
 * @brief Initialize GPIO if not initialized yet
 *
 * Can be called repeatedly. It will initialize GPIO only once.
 */
static void _init_gpio_if_not_initialized_yet(void);

// =========================| High level functions |==========================
bool pm_get_5v_power_supply(void) { return ms_pm_runtime.b_5v_pwr_supply; }

void pm_set_5v_power_supply(bool b_enable) {
  _init_gpio_if_not_initialized_yet();

  ESP_ERROR_CHECK(gpio_set_level(PWR_5V_EN_PIN, b_enable));
}

bool pm_get_can_transceiver(void) { return ms_pm_runtime.b_can_transceiver; }

void pm_set_can_transceiver(bool b_enable) {
  _init_gpio_if_not_initialized_yet();

  ESP_ERROR_CHECK(gpio_set_level(CAN_TRNSCVR_PWR_PIN, b_enable));
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

// ====================| Internal functions: high level |=====================
// ===================| Internal functions: middle level |====================
// =====================| Internal functions: low level |=====================
static void _init_gpio_if_not_initialized_yet(void) {
  // If not initialized yet, initialize
  if (!ms_pm_runtime.b_gpio_initialized) {
    gpio_config_t s_gpio_cfg = {
        .pin_bit_mask = (1ULL << PWR_5V_EN_PIN) | (1ULL << CAN_TRNSCVR_PWR_PIN),
        .mode = GPIO_MODE_OUTPUT,
        .pull_up_en = 0,
        .pull_down_en = 0,
        .intr_type = GPIO_INTR_DISABLE};

    // Configure as output with low (default)
    ESP_ERROR_CHECK(gpio_config(&s_gpio_cfg));

    // Disable all devices - just in case
    ESP_ERROR_CHECK(gpio_set_level(PWR_5V_EN_PIN, 0));
    ESP_ERROR_CHECK(gpio_set_level(CAN_TRNSCVR_PWR_PIN, 0));

    ms_pm_runtime.b_gpio_initialized = true;
  }
}

/**
 * @file
 * @author Martin Stejskal
 * @brief Power manager which allow easily power on/off modules
 */
#ifndef __POWER_MANAGER_H__
#define __POWER_MANAGER_H__
// ===============================| Includes |================================
#include <stdbool.h>
// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================

// ===============================| Functions |===============================

// =========================| High level functions |==========================
/**
 * @brief Tells if +5V power supply is enabled or disabled
 * @return True if enabled, false otherwise
 */
bool pm_get_5v_power_supply(void);

/**
 * @brief Enable or disable +5V power supply
 * @param b_enable If false, disable power supply. Otherwise enable it.
 */
void pm_set_5v_power_supply(bool b_enable);

/**
 * @brief Tells if CAN transceiver is enabled or disabled
 * @return True if enabled, false otherwise
 */
bool pm_get_can_transceiver(void);

/**
 * @brief Enable or disable CAN transceiver
 * @param b_enable If false, disable CAN transceiver. Otherwise enable it.
 */
void pm_set_can_transceiver(bool b_enable);
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

#endif  // __POWER_MANAGER_H__

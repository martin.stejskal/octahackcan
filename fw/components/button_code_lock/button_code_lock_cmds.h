/**
 * @file
 * @author Martin Stejskal
 * @brief Commands for button code lock module
 */
#ifndef __BUTTON_CODE_LOCK_CMDS_H__
#define __BUTTON_CODE_LOCK_CMDS_H__
// ===============================| Includes |================================
#include "button_code_lock.h"
#include "button_code_lock_cfg.h"
// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
/**
 * @brief Structure which says what should be saved and restored
 */
typedef struct {
  // Keep all commands as array with button timing. Every button change should
  // take some time and this time is recorded here. Time here is in ms.
  uint16_t au16available[BTN_CODE_LOCK_EVENT_MAX]
                        [BTN_CODE_LOCK_MAX_NUM_OF_CHANGES];
  // Threshold when command can be evaluated as acceptable
  uint16_t u16diffCoefThreshold;
} tsLoadSaveSettings;
// ===========================| Global variables |============================
/**
 * @brief Default settings for NVS
 *
 * When loading data from NVS and so far no settings was stored, following
 * settings will be loaded
 */
static const tsLoadSaveSettings msDefaultSettings = {
    /**
     * Commands as list of times when button should be pressed and released
     *
     * Button press and release are zig-zag. It makes sense only to record
     * changes. For instance following sequence is given: 500, 1000, 2000, 300,
     * 200. It means that button have to be pressed for 500 ms, then released
     * for 1000 ms, then pressed for 2000 ms, released for 300 ms, pressed for
     * 200 ms and then released.
     *
     * @note Order have to match with teBtnCodeLockEvent enumeration. Also
     * number of changes have to be odd (last record have to be linked with
     *       "pressed" logic.
     *
     */
    .au16available =
        {// Lock
         {1000, 1000, 3000},

         // Unlock
         {1000, 3000, 1000},

         // Set lock combination
         {8000},

         // Set unlock combination
         {5000}},

    // Define threshold when command is still accepted
    .u16diffCoefThreshold = 15000,
};

// ===============================| Functions |===============================
// =========================| High level functions |==========================
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
#endif  // __BUTTON_CODE_LOCK_CMDS_H__

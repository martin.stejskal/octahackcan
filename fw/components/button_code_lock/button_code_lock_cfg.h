/**
 * @file
 * @author Martin Stejskal
 * @brief Configuration for module that keep eye on button code
 */
#ifndef __BUTTON_CODE_LOCK_CFG_H__
#define __BUTTON_CODE_LOCK_CFG_H__
// ===============================| Includes |================================
#include "can_app.h"
// ================================| Defines |================================
// ============================| Default values |=============================

/**
 * @brief Define callback for "lock" event
 *
 * No parameter given. No return value expected
 */
#define BTN_CODE_LOCK_EVENT_LOCK_CALLBACK() can_app_lock()

/**
 * @brief Define callback for "unlock" event
 *
 * No parameter given. No return value expected
 */
#define BTN_CODE_LOCK_EVENT_UNLOCK_CALLBACK() can_app_unlock()

/**
 * @brief How log should be annoying beep when default codes are still used
 *
 * This should enforce user to change default codes. Duration is in ms
 */
#define BTN_CODE_LOCK_ANNOYING_BEEP_DURATION_MS (300)

/**
 * @brief Define maximum number of accepted press and release events
 *
 * Bigger number require more memory and also more requirements to user. Set
 * this value wisely.
 */
#define BTN_CODE_LOCK_MAX_NUM_OF_CHANGES (30)

/**
 * @brief Every button pulse lower than this threshold will be ignored
 *
 * Value is in ms.
 */
#define BTN_CODE_LOCK_MAX_GLITCH_ON_BUTTON_MS (15)

// ==========================| Event notifications |==========================
/**
 * @brief Event "lock" - buzzer configuration
 */
#define BTN_CODE_LOCK_EVENT_LOCK_BUZZER \
  { .u16timeBeepMs = 1000, .u16timeSilenceMs = 0, .u16repeatCount = 0 }

/**
 * @brief Event "unlock" - buzzer configuration
 */
#define BTN_CODE_LOCK_EVENT_UNLOCK_BUZZER \
  { .u16timeBeepMs = 250, .u16timeSilenceMs = 250, .u16repeatCount = 1 }

/**
 * @brief Event "set lock" - buzzer configuration
 */
#define BTN_CODE_LOCK_EVENT_SET_LOCK_BUZZER \
  { .u16timeBeepMs = 500, .u16timeSilenceMs = 50, .u16repeatCount = 2 }

/**
 * @brief Event "set lock" - LED configuration
 */
#define BTN_CODE_LOCK_EVENT_SET_LOCK_LED \
  { .u8pwmR = 180, .u8pwmG = 0, .u8pwmB = 255 }

/**
 * @brief Event "set unlock" - buzzer configuration
 */
#define BTN_CODE_LOCK_EVENT_SET_UNLOCK_BUZZER \
  { .u16timeBeepMs = 70, .u16timeSilenceMs = 50, .u16repeatCount = 4 }

/**
 * @brief Event "set unlock" - LED configuration
 */
#define BTN_CODE_LOCK_EVENT_SET_UNLOCK_LED \
  { .u8pwmR = 0, .u8pwmG = 255, .u8pwmB = 180 }
// ==========================| Preprocessor checks |==========================
#endif  // __BUTTON_CODE_LOCK_CFG_H__

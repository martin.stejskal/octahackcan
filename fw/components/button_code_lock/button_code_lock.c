/**
 * @file
 * @author Martin Stejskal
 * @brief Module that keep eye on button code
 */
// ===============================| Includes |================================
#include <assert.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <string.h>

// ESP specific
#include <esp32/rtc.h>
#include <esp_log.h>

#include "button_code_lock.h"
#include "button_code_lock_cfg.h"
#include "button_code_lock_cmds.h"
#include "buzzer.h"
#include "led_rgb.h"
#include "nvs_settings.h"
// ================================| Defines |================================
/**
 * @brief Returns number of items from given array
 */
#define GET_NUM_OF_TIEMS(array) (sizeof(array) / sizeof(array[0]))
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
#if BTN_CODE_LOCK_MAX_NUM_OF_CHANGES > 255
#error "Sorry, maximum supported changes have to fit into 8 bit number"
#endif

#if BTN_CODE_LOCK_EVENT_MAX > 255
#error "Sorry, maximum supported codes is 255"
#endif
// ===========================| Structures, enums |===========================
typedef enum {
  BTN_CODE_LOCK_STATE_WAITING_FOR_CMD = 0,  //!< Waiting for command
  BTN_CODE_LOCK_STATE_LEARN_LOCK_CMD,       //!< Learn lock command
  BTN_CODE_LOCK_STATE_LEARN_UNLOCK_CMD,     //!< Learn unlock command
} teStates;

/**
 * @brief Define structure for common runtime variables
 */
typedef struct {
  struct {
    // Time when CPU wakes up from deep sleep or after power on. Value is in us
    uint64_t u64startUs;

    // Time when previous button change was done. Value is in us.
    uint64_t u64previousChangeUs;

    // Time when current change was registered. Value is in us.
    uint64_t u64currentChangeUs;
  } time;

  struct {
    // Keep all commands as array with button timing. Every button change should
    // take some time and this time is recorded here. Time here is in ms.
    uint16_t au16available[BTN_CODE_LOCK_EVENT_MAX]
                          [BTN_CODE_LOCK_MAX_NUM_OF_CHANGES];

    // Command length at one place. This will be filled during initialization.
    // It will help to recognize commands more easily.
    uint8_t au8availableLen[BTN_CODE_LOCK_EVENT_MAX];

    // When user pressing and release code lock button, program need to storage
    // somewhere times when buttons were pressed
    uint16_t au16user[BTN_CODE_LOCK_MAX_NUM_OF_CHANGES];

    // Define current user command length. Help with processing
    uint8_t u8userLen;

    // Threshold when command can be evaluated as acceptable
    uint16_t u16diffCoefThreshold;
  } cmds;

  // Tells which level is expected on next callback. This helps to filer
  // glitches.
  teBtnCodeLockState eExpectedLevel;

  // ID for NVM operations
  const char *pacNvmId;

  // State for main state machine
  teStates eState;

} tsRuntime;

// ===========================| Global variables |============================
/**
 * @brief Tag for logger
 */
static const char *TAG = "BtnCodeLock";

// list of user commands (their index actually) - compare with default
static const uint16_t mau16userCmdsIdxs[] = {BTN_CODE_LOCK_EVENT_LOCK,
                                             BTN_CODE_LOCK_EVENT_UNLOCK};

// List of "set" commands which should be cleared (erased). Well, their
// indexes, to make it easy
static const uint16_t mau16setCmdsIdxs[] = {
    BTN_CODE_LOCK_EVENT_SET_LOCK_COMBINATION,
    BTN_CODE_LOCK_EVENT_SET_UNLOCK_COMBINATION};

/**
 * @brief Keep all important runtime variables
 */
static tsRuntime msRuntime = {
    .pacNvmId = "btnCodeLock",
};
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
// ========================| State: wait for command |========================
/**
 * @brief Handle state "wait for command"
 * @param[in, out] psRuntime Pointer to runtime variables
 */
static void _stateWaitForCmd(tsRuntime *psRuntime);

/**
 * @brief Process "lock" event
 *
 * Do beep sound and execute callback function
 */
static void _processLockEvent(void);

/**
 * @brief Process "unlock" event
 *
 * Do beep sound and execute callback function
 */
static void _processUnlockEvent(void);

/**
 * @brief Process "set lock" event
 *
 * Switch state machine to learning mode and clean up user input buffer
 *
 * @param[out] psRuntime psRuntime Pointer to runtime structure
 */
static void _processSetLockCombinationEvent(tsRuntime *psRuntime);

/**
 * @brief Process "set unlock" event
 *
 * @param[out] psRuntime Pointer to runtime structure
 */
static void _processSetUnLockCombinationEvent(tsRuntime *psRuntime);

// ===========================| State: learn lock |===========================
/**
 * @brief Learn lock code
 *
 * Collect user input and store it into NVS
 *
 * @param[in, out] psRuntime Pointer to runtime structure
 */
static void _stateLearnLockCmd(tsRuntime *psRuntime);

// ==========================| State: learn unlock |==========================
/**
 * @brief Learn unlock code
 *
 * Collect user input and store it into NVS
 *
 * @param[in, out] psRuntime Pointer to runtime structure
 */
static void _stateLearnUnlockCmd(tsRuntime *psRuntime);

// =======================| Other internal functions |========================
/**
 * @brief Initialize runtime variables
 *
 * @param[in, out] psRuntime Pointer to runtime structure
 */
static void _initializeRuntimeVariablesLoadSettings(tsRuntime *psRuntime);

/**
 * @brief Annoy user to set non-default codes
 *
 * If there are still default codes, annoy user to change it
 *
 * @param[in, out] psRuntime Pointer to runtime structure
 */
static void _makeAnnoyingSoundIfUserDidNotDefinedCommands(tsRuntime *psRuntime);

/**
 * @brief Store how long was button pressed or released
 *
 * Calculate how long was button pressed or released and write that value into
 * "user command" array. Also take care about glitches.
 *
 * @param[in, out] psRuntime Pointer to runtime structure
 * @return ESP_OK if no error, ESP_ERR_INVALID_RESPONSE in case that glitch is
 *         detected and ESP_ERR_NO_MEM if "user command" array is full.
 */
static esp_err_t _saveButtonChangeDuration(tsRuntime *psRuntime);

/**
 * @brief Try to decode command from user input
 * @param[in] psRuntime Pointer to runtime structure
 * @return BTN_CODE_LOCK_EVENT_MAX if user input does not match with any
 *         command. Otherwise it return appropriate value for input command
 */
static teBtnCodeLockEvent _decodeUserCmd(const tsRuntime *psRuntime);

/**
 * @brief Check if all user commands are already changed
 *
 * Due to security it is good idea change all commands which user can change.
 *
 *
 * @param[in] psRuntime Pointer to runtime structure
 * @return If all changeable commands are already changed, returns true. False
 *         otherwise.
 */
static bool _areAllUserCommandsRedefined(const tsRuntime *psRuntime);

/**
 * @brief Disable all commands that allow set any code
 *
 * When all "user commands" are redefined, it does not make sense (from
 * security point of view) to allow overwrite already set commands. Therefore
 * at some point is required to disable these "set" commands.
 *
 * @param[out] psRuntime Pointer to runtime structure
 */
static void _disableAllSetCmds(tsRuntime *psRuntime);

/**
 * @brief Compare reference and user command and get difference value
 *
 * @param[in] au16reference Reference command as array
 * @param[in] au16compared Comparing command as array
 * @return Objective difference between reference and given command. Higher
 *         number means bigger difference. Lower number means "more close"
 *         to reference
 */
static uint16_t _compareCommandsGetDiff(
    const uint16_t au16reference[static BTN_CODE_LOCK_MAX_NUM_OF_CHANGES],
    const uint16_t au16compared[static BTN_CODE_LOCK_MAX_NUM_OF_CHANGES]);

/**
 * @brief Shift reference and compared word to be "always same"
 *
 * Shift reference and compared word to the left until one of them have "1"
 * ons MSb.
 *
 * @param[in, out] pu16reference Pointer to reference word
 * @param[in, out] pu16compared Pointer to compared word
 */
static void _shiftReferenceAndCompared(uint16_t *pu16reference,
                                       uint16_t *pu16compared);

/**
 * @brief Recalculate length of all "available commands"
 *
 * In runtime structure recalculate command length for all "available commands"
 *
 * @param[in, out] psRuntime Pointer to runtime structure
 */
static void _updateAvailableCmdsLength(tsRuntime *psRuntime);

/**
 * @brief Clean up "user command" input
 *
 * @param[out] psRuntime Pointer to runtime structure
 */
static void _cleanUserCommandInput(tsRuntime *psRuntime);

/**
 * @brief Load settings from NVS
 *
 * @param[out] psRuntime Pointer to runtime structure
 */
static void _loadSettings(tsRuntime *psRuntime);

/**
 * @brief Save settings to NVS
 *
 * @param[in] psRuntime Pointer to runtime structure
 */
static void _saveSettings(const tsRuntime *psRuntime);

/**
 * @brief Print current settings at info level
 *
 * @param[in] psRuntime Pointer to runtime structure
 */
static void _printSettings(const tsRuntime *psRuntime);

// =========================| High level functions |==========================
void buttonCodeLockInitialize(void) {
  // Power on or wake up from deep sleep. Anyway this is first button event.
  _initializeRuntimeVariablesLoadSettings(&msRuntime);

  // If there are still default command codes, annoy user
  _makeAnnoyingSoundIfUserDidNotDefinedCommands(&msRuntime);
}
// ========================| Middle level functions |=========================
void buttonCodeLockRestoreDefaults(void) {
  // Restore all available commands
  memcpy(msRuntime.cmds.au16available, msDefaultSettings.au16available,
         sizeof(msRuntime.cmds.au16available));

  msRuntime.cmds.u16diffCoefThreshold = msDefaultSettings.u16diffCoefThreshold;

  // Update command lengths information
  _updateAvailableCmdsLength(&msRuntime);

  // Erase data in NVS
  nvsSettingsDel(TAG);
}

void buttonCodeLockSetCommandTolerance(uint8_t u8toleranceLevel) {
  // Working value is 16 bit long. But we allow user to define it less
  // accurate. So let's use only upper 8 bits
  msRuntime.cmds.u16diffCoefThreshold = u8toleranceLevel << 8;

  ESP_LOGD(TAG, "New tolerance level: %d (16 bit)",
           msRuntime.cmds.u16diffCoefThreshold);

  // And save settings
  _saveSettings(&msRuntime);
}

uint8_t buttonCodeLockGetCommandTolerance(void) {
  // User have ability to configure it with smaller resolution (to make it
  // easier). So only upper 8 bits are actually used
  return (msRuntime.cmds.u16diffCoefThreshold >> 8);
}
// ==========================| Low level functions |==========================
void buttonCodeLockLevelChangeCallback(const teBtnCodeLockState eButtonStatus) {
  // Get current time and record it
  msRuntime.time.u64currentChangeUs = esp_rtc_get_time_us();

  // If this is first button press
  if (msRuntime.time.u64startUs == 0) {
    // Expect that current time was recorded already
    msRuntime.time.u64startUs = msRuntime.time.u64currentChangeUs;

    // There was not recorded previous change yet. Use current timestamp
    msRuntime.time.u64previousChangeUs = msRuntime.time.u64currentChangeUs;

    // If button is pressed now, we will expect released next time
    if (eButtonStatus == BTN_CODE_LOCK_PRESSED) {
      // Now pressed -> expect released state next time
      msRuntime.eExpectedLevel = BTN_CODE_LOCK_RELEASED;

    } else if (eButtonStatus == BTN_CODE_LOCK_RELEASED) {
      // Now released -> expect pressed state next time
      msRuntime.eExpectedLevel = BTN_CODE_LOCK_PRESSED;

    } else {
      // Internal error. This should not happen
      assert(0);
    }

    ESP_LOGI(TAG, "Registered first button change. Level: %d @ %llu",
             eButtonStatus, msRuntime.time.u64currentChangeUs);

    // First change -> need to return to avoid further processing
    return;
  }

  ESP_LOGI(TAG, "Code button level: %d (Expected: %d) @ %llu", eButtonStatus,
           msRuntime.eExpectedLevel, msRuntime.time.u64currentChangeUs);

  // If receive button level which is not expected, ignore it
  if (msRuntime.eExpectedLevel != eButtonStatus) {
    ESP_LOGW(TAG, "Button have unexpected state (expected opposite)");
    return;
  }

  // Calculate how long was button pressed/released and update user command
  // length
  if (_saveButtonChangeDuration(&msRuntime)) {
    // For some reason can not deal with new button changes -> return
    return;
  }

  switch (msRuntime.eState) {
    case BTN_CODE_LOCK_STATE_WAITING_FOR_CMD:
      _stateWaitForCmd(&msRuntime);
      break;
    case BTN_CODE_LOCK_STATE_LEARN_LOCK_CMD:
      _stateLearnLockCmd(&msRuntime);
      break;
    case BTN_CODE_LOCK_STATE_LEARN_UNLOCK_CMD:
      _stateLearnUnlockCmd(&msRuntime);
      break;
  }
  // Store current time change as previous. Will be used in next run
  msRuntime.time.u64previousChangeUs = msRuntime.time.u64currentChangeUs;
}

uint8_t buttonCodeLockGetCommandLength(
    const uint16_t au16cmd[static BTN_CODE_LOCK_MAX_NUM_OF_CHANGES]) {
  for (uint8_t u8length = 0; u8length < BTN_CODE_LOCK_MAX_NUM_OF_CHANGES;
       u8length++) {
    // If found terminating character, exit
    if (au16cmd[u8length] == 0) {
      return u8length;
    }
  }

  // If reached maximum value, we can not go further anyway.
  return BTN_CODE_LOCK_MAX_NUM_OF_CHANGES;
}

// =============================| Print related |=============================
const char *buttonCodeLockGetEventName(teBtnCodeLockEvent eEvent) {
  const static char *pacLock = "Lock";
  const static char *pacUnlock = "Unlock";
  const static char *pacSetLock = "Set lock";
  const static char *pacSetUnlock = "Set unlock";
  const static char *pacMax = "MAX";

  switch (eEvent) {
    case BTN_CODE_LOCK_EVENT_LOCK:
      return pacLock;
    case BTN_CODE_LOCK_EVENT_UNLOCK:
      return pacUnlock;
    case BTN_CODE_LOCK_EVENT_SET_LOCK_COMBINATION:
      return pacSetLock;
    case BTN_CODE_LOCK_EVENT_SET_UNLOCK_COMBINATION:
      return pacSetUnlock;
    case BTN_CODE_LOCK_EVENT_MAX:
      return pacMax;
  }

  // This should not happen. Case above should handle all the values.
  return "ERROR";
}

// ==========================| Internal functions |===========================
// ========================| State: wait for command |========================
static void _stateWaitForCmd(tsRuntime *psRuntime) {
  assert(psRuntime->eState == BTN_CODE_LOCK_STATE_WAITING_FOR_CMD);

  // Try to decode user command
  teBtnCodeLockEvent eDecodedCmd = _decodeUserCmd(psRuntime);

  if (eDecodedCmd < BTN_CODE_LOCK_EVENT_MAX) {
    ESP_LOGI(TAG, "Detected command >>%s<<",
             buttonCodeLockGetEventName(eDecodedCmd));
  }

  switch (eDecodedCmd) {
    case BTN_CODE_LOCK_EVENT_LOCK:
      _processLockEvent();

      // Event finished. Reset logic
      _initializeRuntimeVariablesLoadSettings(psRuntime);
      break;
    case BTN_CODE_LOCK_EVENT_UNLOCK:
      _processUnlockEvent();

      // Event finished. Reset logic
      _initializeRuntimeVariablesLoadSettings(psRuntime);
      break;
    case BTN_CODE_LOCK_EVENT_SET_LOCK_COMBINATION:
      _processSetLockCombinationEvent(psRuntime);
      break;
    case BTN_CODE_LOCK_EVENT_SET_UNLOCK_COMBINATION:
      _processSetUnLockCombinationEvent(psRuntime);
      break;
    case BTN_CODE_LOCK_EVENT_MAX:
      // This means "unknown command" or command not finished yet
      break;
  }
}

static void _processLockEvent(void) {
  // Make sound. Do not care about error code
  tsBuzzerBeepCfg sBuzzerCfg = BTN_CODE_LOCK_EVENT_LOCK_BUZZER;
  buzzerBeep(&sBuzzerCfg);

  // Call custom function
  BTN_CODE_LOCK_EVENT_LOCK_CALLBACK();
}

static void _processUnlockEvent(void) {
  // Make sound. Do not care about error code
  tsBuzzerBeepCfg sBuzzerCfg = BTN_CODE_LOCK_EVENT_UNLOCK_BUZZER;
  buzzerBeep(&sBuzzerCfg);

  // Call custom function
  BTN_CODE_LOCK_EVENT_UNLOCK_CALLBACK();
}

static void _processSetLockCombinationEvent(tsRuntime *psRuntime) {
  // Setup RGB LED -> inform user
  teLedRGBchannels sColor = BTN_CODE_LOCK_EVENT_SET_LOCK_LED;
  ledRGBset(sColor);

  // Make sound. Do not care about error code
  tsBuzzerBeepCfg sBuzzerCfg = BTN_CODE_LOCK_EVENT_SET_LOCK_BUZZER;
  buzzerBeep(&sBuzzerCfg);

  // Clean up user command input, so it can store new code
  _cleanUserCommandInput(psRuntime);

  // Also need to reset assumed level, start time and so on
  psRuntime->time.u64startUs = 0;

  // Change state in main state machine
  psRuntime->eState = BTN_CODE_LOCK_STATE_LEARN_LOCK_CMD;

  ESP_LOGI(TAG, "Ready to learn >>Lock<< pattern");
}

static void _processSetUnLockCombinationEvent(tsRuntime *psRuntime) {
  // Setup RGB LED -> inform user
  teLedRGBchannels sColor = BTN_CODE_LOCK_EVENT_SET_UNLOCK_LED;
  ledRGBset(sColor);

  // Make sound. Do not care about error code
  tsBuzzerBeepCfg sBuzzerCfg = BTN_CODE_LOCK_EVENT_SET_UNLOCK_BUZZER;
  buzzerBeep(&sBuzzerCfg);

  // Clean up user command input, so it can store new code
  _cleanUserCommandInput(psRuntime);

  // Also need to reset assumed level, start time and so on
  psRuntime->time.u64startUs = 0;

  // Change state in main state machine
  psRuntime->eState = BTN_CODE_LOCK_STATE_LEARN_UNLOCK_CMD;

  ESP_LOGI(TAG, "Ready to learn >>Unlock<< pattern");
}

// ===========================| State: learn lock |===========================
static void _stateLearnLockCmd(tsRuntime *psRuntime) {
  // Sizes of these two arrays should be same. From available just taking
  // first item. Does not really matter.
  assert(sizeof(psRuntime->cmds.au16available[0]) ==
         sizeof(psRuntime->cmds.au16user));

  // Copy current user code to "available commands"
  memcpy(psRuntime->cmds.au16available[BTN_CODE_LOCK_EVENT_LOCK],
         psRuntime->cmds.au16user, sizeof(psRuntime->cmds.au16user));

  // Save code to the NVS
  _saveSettings(psRuntime);
}

// ==========================| State: learn unlock |==========================
static void _stateLearnUnlockCmd(tsRuntime *psRuntime) {
  // Sizes of these two arrays should be same. From available just taking
  // first item. Does not really matter.
  assert(sizeof(psRuntime->cmds.au16available[0]) ==
         sizeof(psRuntime->cmds.au16user));

  // Copy current user code to "available commands"
  memcpy(psRuntime->cmds.au16available[BTN_CODE_LOCK_EVENT_UNLOCK],
         psRuntime->cmds.au16user, sizeof(psRuntime->cmds.au16user));

  // Save code to the NVS
  _saveSettings(psRuntime);
}

// =======================| Other internal functions |========================
static void _initializeRuntimeVariablesLoadSettings(tsRuntime *psRuntime) {
  // Reset state machine
  psRuntime->eState = BTN_CODE_LOCK_STATE_WAITING_FOR_CMD;

  // Time related
  // Reset initial time stamps
  psRuntime->time.u64startUs = 0;

  // Command related
  _loadSettings(psRuntime);

  // Check if other code commands are set. If yes, disable command which allow
  // change settings
  if (_areAllUserCommandsRedefined(psRuntime)) {
    _disableAllSetCmds(psRuntime);
  }

  // Just to inform developer
  _printSettings(psRuntime);

  // Clean up user input pattern
  _cleanUserCommandInput(psRuntime);

  // Get commands length
  _updateAvailableCmdsLength(psRuntime);
}

static void _makeAnnoyingSoundIfUserDidNotDefinedCommands(
    tsRuntime *psRuntime) {
  assert(psRuntime);

  ledTurnOffAll();

  // Go through all commands and if some command is still same, make annoying
  // sound. Otherwise just pass
  for (teBtnCodeLockEvent eEventIdx = (teBtnCodeLockEvent)0;
       eEventIdx < BTN_CODE_LOCK_EVENT_MAX; eEventIdx++) {
    if (_compareCommandsGetDiff(psRuntime->cmds.au16available[eEventIdx],
                                msDefaultSettings.au16available[eEventIdx]) ==
        0) {
      buzzerSingleBeep(BTN_CODE_LOCK_ANNOYING_BEEP_DURATION_MS);

      ESP_LOGW(TAG, "Still using default codes! You should redefine them!");

      // Light "yellow" (red + green). Keep it shining no matter what
      ledRGBsetRlvl(LED_LVL_MAX);
      ledRGBsetGlvl(LED_LVL_MAX);

      return;
    }
  }

  // If code went here, configuration is fine.
}

static esp_err_t _saveButtonChangeDuration(tsRuntime *psRuntime) {
  // Check if user buffer is full. If full, can not fill more data
  if (psRuntime->cmds.u8userLen >= BTN_CODE_LOCK_MAX_NUM_OF_CHANGES) {
    ESP_LOGE(TAG,
             "Can not process user command. Buffer is full. "
             "Command should be shorter.");
    return ESP_ERR_NO_MEM;
  }

  // Calculate state (pressed/released) duration. Value will be in us
  uint64_t u64durationUs =
      psRuntime->time.u64currentChangeUs - psRuntime->time.u64previousChangeUs;

  // For our purpose it is enough to have value in ms -> divide by 1000
  u64durationUs /= 1000;

  // Need to fit into 16 bit value. Expecting that intervals will be not
  // be bigger than 10 seconds in real life. So 1 minute or more is simply
  // something which should not be used
  assert(u64durationUs < 0xFFFF);

  // Duration is now in ms
  if (u64durationUs <= BTN_CODE_LOCK_MAX_GLITCH_ON_BUTTON_MS) {
    ESP_LOGW(TAG, "Glitch detected (%d ms). Ignoring", (uint16_t)u64durationUs);
    return ESP_ERR_INVALID_RESPONSE;
  } else {
    // It is not glitch -> toggle expected level on pin for next time
    if (psRuntime->eExpectedLevel == BTN_CODE_LOCK_PRESSED) {
      psRuntime->eExpectedLevel = BTN_CODE_LOCK_RELEASED;

    } else {
      psRuntime->eExpectedLevel = BTN_CODE_LOCK_PRESSED;
    }
  }

  // Write to appropriate slot and increase user command length
  psRuntime->cmds.au16user[psRuntime->cmds.u8userLen] = (uint16_t)u64durationUs;

  ESP_LOGD(TAG, "Button pressed/released for %d ms",
           psRuntime->cmds.au16user[psRuntime->cmds.u8userLen]);

  // Value added -> length increased by 1
  psRuntime->cmds.u8userLen++;

  return ESP_OK;
}

static teBtnCodeLockEvent _decodeUserCmd(const tsRuntime *psRuntime) {
  // Default value - "unknown command"
  teBtnCodeLockEvent eCmd = BTN_CODE_LOCK_EVENT_MAX;

  // Fast comparison is testing command length
  bool bUserCmdLengthMatchWithSomeCmd = false;

  // Current difference coefficient for current command
  uint16_t u16diffCoeficient;
  // Track minimum difference coefficient in order to guess command more
  // precisely. Have to be set to highest possible value during initialization
  uint16_t u16diffCoefficientMin = -1;

  for (teBtnCodeLockEvent eEventIdx = (teBtnCodeLockEvent)0;
       eEventIdx < BTN_CODE_LOCK_EVENT_MAX; eEventIdx++) {
    // If at least one command might be potentially there, try it
    if (psRuntime->cmds.u8userLen ==
        psRuntime->cmds.au8availableLen[eEventIdx]) {
      bUserCmdLengthMatchWithSomeCmd = true;
      break;
    }
  }

  // If length of user command does not match with length of any known command
  if (!bUserCmdLengthMatchWithSomeCmd) {
    // This value means "unknown command"
    return eCmd;
  }

  // OK, so if we're here, there is at least some chance that command is
  // just fine. Need to compare it with known commands
  for (teBtnCodeLockEvent eEventIdx = (teBtnCodeLockEvent)0;
       eEventIdx < BTN_CODE_LOCK_EVENT_MAX; eEventIdx++) {
    u16diffCoeficient = _compareCommandsGetDiff(
        psRuntime->cmds.au16available[eEventIdx], psRuntime->cmds.au16user);
    ESP_LOGD(TAG, "Diff coef for %s: %d", buttonCodeLockGetEventName(eEventIdx),
             u16diffCoeficient);

    // If coefficient is below threshold, it is technically valid command.
    // Still process will search through all possible commands. More suitable
    // command (lower difference coefficient) can be found.
    if (u16diffCoeficient < psRuntime->cmds.u16diffCoefThreshold) {
      // New minimum found -> store command
      if (u16diffCoeficient < u16diffCoefficientMin) {
        u16diffCoefficientMin = u16diffCoeficient;
        eCmd = eEventIdx;
      }
    }
  }

  return eCmd;
}

static bool _areAllUserCommandsRedefined(const tsRuntime *psRuntime) {
  // Iterate over items in mau16userCmdsIdxs
  for (uint8_t u8itemIdx = 0; u8itemIdx < GET_NUM_OF_TIEMS(mau16userCmdsIdxs);
       u8itemIdx++) {
    // Get index which can be used for addressing in default and runtime array
    uint8_t u8comparedCmdIdx = mau16userCmdsIdxs[u8itemIdx];

    // Compare commands. If commands are identical, result from compare
    // function will be 0. Otherwise it will say how much are they different,
    // but we do not need to know now
    if (_compareCommandsGetDiff(
            msDefaultSettings.au16available[u8comparedCmdIdx],
            psRuntime->cmds.au16available[u8comparedCmdIdx])) {
      // Commands differs
    } else {
      // They're same -> not all user commands are redefined
      return false;
    }
  }

  return true;
}

static void _disableAllSetCmds(tsRuntime *psRuntime) {
  // Iterate over items in mau16setCmdsIdxs
  for (uint8_t u8itemIdx = 0; u8itemIdx < GET_NUM_OF_TIEMS(mau16setCmdsIdxs);
       u8itemIdx++) {
    // Get index which can be used for addressing in default and runtime array
    uint8_t u8comparedCmdIdx = mau16setCmdsIdxs[u8itemIdx];

    // Clean it up
    memset(psRuntime->cmds.au16available[u8comparedCmdIdx], 0,
           sizeof(psRuntime->cmds.au16available[u8comparedCmdIdx]));

    // Also change length value
    psRuntime->cmds.au8availableLen[u8comparedCmdIdx] = 0;
  }
}

static uint16_t _compareCommandsGetDiff(
    const uint16_t au16reference[static BTN_CODE_LOCK_MAX_NUM_OF_CHANGES],
    const uint16_t au16compared[static BTN_CODE_LOCK_MAX_NUM_OF_CHANGES]) {
  // Basically tells how big is difference in average
  uint32_t u32diffCoeficient = 0;

  // Difference for one iteration
  uint16_t u16diff;

  // Temporary variables for processing data
  uint16_t u16referencePulse;
  uint16_t u16comparedPulse;

  // Compare value by value. Stop when read whole reference array or when found
  // zero value at in reference array.
  uint8_t u8changeIdx;
  for (u8changeIdx = 0; u8changeIdx < BTN_CODE_LOCK_MAX_NUM_OF_CHANGES;
       u8changeIdx++) {
    // If value is zero, break it - reference code was read up to the end
    if (au16reference[u8changeIdx] == 0) {
      // If reference command is "zero clicks" long, it means that is disabled
      // and therefore definitely not the same
      if (u8changeIdx == 0) {
        // Maximum difference number
        return 0xFFFF;
      }

      break;
    }

    if (au16compared[u8changeIdx] == 0) {
      // Reference is not zero, but compared is -> they definitely does not
      // match -> return maximum possible difference value
      return 0xFFFF;
    }

    // Load values to temporary variables, so they can be processed
    u16referencePulse = au16reference[u8changeIdx];
    u16comparedPulse = au16compared[u8changeIdx];

    // Now it is need to shift values to the left, so very first "1" will be
    // on the MSbit position. This way it dynamically allow compare values from
    // different ranges with similar policy.
    _shiftReferenceAndCompared(&u16referencePulse, &u16comparedPulse);

    // Difference between reference and compared basically tells how much is
    // compared trustworthy. Since using unsigned value to express "trust",
    // the difference have to be absolute value.
    if (u16referencePulse > u16comparedPulse) {
      u16diff = u16referencePulse - u16comparedPulse;
    } else {
      // Compared > reference
      u16diff = u16comparedPulse - u16referencePulse;
    }

    // Add "trust" for one value into accumulator. Trust is opposite of
    // difference
    u32diffCoeficient += u16diff;
  }

  // Recalculate trust coefficient to relative value (consider command length)
  u32diffCoeficient /= u8changeIdx;

  assert(u32diffCoeficient <= 0xFFFF);

  return (uint16_t)u32diffCoeficient;
}

static void _shiftReferenceAndCompared(uint16_t *pu16reference,
                                       uint16_t *pu16compared) {
  // Values should not be zeros
  assert(*pu16reference);
  assert(*pu16compared);

  bool bFoundOne = false;
  uint16_t u16oneOnMsbReference, u16oneOnMsbCompared;

  // Keep searching until leading "1" is found
  while (!bFoundOne) {
    // Value is 16 bit long -> easy to test
    u16oneOnMsbReference = *pu16reference & 0x8000;
    u16oneOnMsbCompared = *pu16compared & 0x8000;

    if (u16oneOnMsbReference || u16oneOnMsbCompared) {
      // There is leading one in reference value or in compared sample on MSbit
      // position -> found
      return;
    } else {
      // Shift both to the left and check again
      *pu16reference = *pu16reference << 1;
      *pu16compared = *pu16compared << 1;
    }
  }
}

static void _updateAvailableCmdsLength(tsRuntime *psRuntime) {
  for (teBtnCodeLockEvent eEventIdx = (teBtnCodeLockEvent)0;
       eEventIdx < BTN_CODE_LOCK_EVENT_MAX; eEventIdx++) {
    psRuntime->cmds.au8availableLen[eEventIdx] = buttonCodeLockGetCommandLength(
        psRuntime->cmds.au16available[eEventIdx]);
  }
}

static void _cleanUserCommandInput(tsRuntime *psRuntime) {
  memset(psRuntime->cmds.au16user, 0, sizeof(psRuntime->cmds.au16user));
  psRuntime->cmds.u8userLen = 0;
}

static void _loadSettings(tsRuntime *psRuntime) {
  // Structure for saving/loading data from NVS
  tsLoadSaveSettings sStoredData;

  // Load command patterns
  if (nvsSettingsGet(TAG, &sStoredData, &msDefaultSettings,
                     sizeof(sStoredData))) {
    // Loading from NVM failed, but what can we do
    ESP_LOGE(TAG, "Loading commands from NVS failed :(");
  } else {
    // Loading was successful -> update runtime variables
    memcpy(&psRuntime->cmds.au16available, &sStoredData.au16available,
           sizeof(psRuntime->cmds.au16available));

    psRuntime->cmds.u16diffCoefThreshold = sStoredData.u16diffCoefThreshold;
  }
}

static void _saveSettings(const tsRuntime *psRuntime) {
  // Structure for saving/loading data from NVS
  tsLoadSaveSettings sStoredData;

  memcpy(&sStoredData.au16available, &psRuntime->cmds.au16available,
         sizeof(sStoredData.au16available));

  sStoredData.u16diffCoefThreshold = psRuntime->cmds.u16diffCoefThreshold;

  if (nvsSettingsSet(TAG, &sStoredData, sizeof(sStoredData))) {
    // If saving fails, there is not much we can do
    ESP_LOGE(TAG, "Saving setting failed :(");
  }
}

static void _printSettings(const tsRuntime *psRuntime) {
  ESP_LOGI(TAG, "=== Code lock settings ===");
  ESP_LOGI(TAG, "Threshold: %d", psRuntime->cmds.u16diffCoefThreshold);

  // Temporary variable for creating final string when describing event.
  // It will contain event name (20 chars max) + show command time stamps
  // (16 bit -> 5 characters) separated by comma (1 character)
  char acEvent[20 + 5 * BTN_CODE_LOCK_MAX_NUM_OF_CHANGES +
               1 * BTN_CODE_LOCK_MAX_NUM_OF_CHANGES];

  // Store timestamp as integer
  uint16_t u16timestamp;

  // Store timestamp as string. 5 digits + termination character + comma
  char acTimestamp[7];

  // Go through all events
  for (teBtnCodeLockEvent eEvent = (teBtnCodeLockEvent)0;
       eEvent < BTN_CODE_LOCK_EVENT_MAX; eEvent++) {
    // Start with event name
    sprintf(acEvent, "%s : ", buttonCodeLockGetEventName(eEvent));

    // Then start adding time stamps into event string
    for (uint8_t u8timestampIdx = 0;
         u8timestampIdx < BTN_CODE_LOCK_MAX_NUM_OF_CHANGES; u8timestampIdx++) {
      // Load time stamp
      u16timestamp = psRuntime->cmds.au16available[eEvent][u8timestampIdx];

      // If time stamp is non zero, add it as value
      if (u16timestamp) {
        // Add timestamp as value into final string
        sprintf(acTimestamp, "%d,", u16timestamp);
        strcat(acEvent, acTimestamp);

      } else {
        // This is terminating character, print "EOC" (end of code)
        strcat(acEvent, "EOC");

        // Nothing more to process
        break;
      }
    }  // for all time stamps

    // Print settings for current event
    ESP_LOGI(TAG, "%s", acEvent);
  }
}

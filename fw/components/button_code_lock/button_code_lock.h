/**
 * @file
 * @author Martin Stejskal
 * @brief Module that keep eye on button code
 */
#ifndef __BUTTON_CODE_LOCK_H__
#define __BUTTON_CODE_LOCK_H__
// ===============================| Includes |================================
#include "button_code_lock_cfg.h"
// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
typedef enum {
  BTN_CODE_LOCK_RELEASED = 0,  //!< BTN_CODE_LOCK_RELEASED Button released
  BTN_CODE_LOCK_PRESSED = 1    //!< BTN_CODE_LOCK_PRESSED Button pressed
} teBtnCodeLockState;

typedef enum {
  // User can redefine following commands
  BTN_CODE_LOCK_EVENT_LOCK,
  BTN_CODE_LOCK_EVENT_UNLOCK,

  // Following commands will be deactivated, once all above will be defined
  BTN_CODE_LOCK_EVENT_SET_LOCK_COMBINATION,
  BTN_CODE_LOCK_EVENT_SET_UNLOCK_COMBINATION,

  BTN_CODE_LOCK_EVENT_MAX,
} teBtnCodeLockEvent;
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =========================| High level functions |==========================
/**
 * @brief Initialize button code lock logic
 */
void buttonCodeLockInitialize(void);
// ========================| Middle level functions |=========================
/**
 * @brief Reset all user defined codes
 *
 * Once called, it is applied and settings is restored also in NVS.
 */
void buttonCodeLockRestoreDefaults(void);

/**
 * @brief Set tolerance level
 *
 * When processing commands, it is expected some inaccuracy from user. Because,
 * let's be honest, who press button with 1 ms accuracy? So this value
 * basically says how "off" user input can be. Practical value is about 60.
 * If you're really bad in timing, increase it. If you're paranoid, decrease
 * it.
 *
 * @note Once called, settings is stored in NVS
 *
 * @param u8toleranceLevel Tolerance level. Higher value means higher
 *                         tolerance. Recommended value is 60.
 */
void buttonCodeLockSetCommandTolerance(uint8_t u8toleranceLevel);

/**
 * @brief Get tolerance level
 *
 * For bit more detailed description, take a look to
 * @ref buttonCodeLockSetCommandTolerance
 *
 * @return Tolerance level. Higher value means higher tolerance. Practical
 *         value is about 60.
 */
uint8_t buttonCodeLockGetCommandTolerance(void);
// ==========================| Low level functions |==========================
/**
 * @brief Call it when level change is detected on dedicated pin
 * @param eButtonStatus Button status (pressed/released). Does not have to
 *        necessary correspond with logical one or zero.
 */
void buttonCodeLockLevelChangeCallback(const teBtnCodeLockState eButtonStatus);

/**
 * @brief Return length of given command
 * @param au16cmd Command as data array (time values in ms in array)
 * @return Command length
 */
uint8_t buttonCodeLockGetCommandLength(
    const uint16_t au16cmd[static BTN_CODE_LOCK_MAX_NUM_OF_CHANGES]);

// =============================| Print related |=============================
/**
 * @brief Return event name as string
 * @param eEvent Event code as enumeration
 * @return Pointer to string - event name
 */
const char *buttonCodeLockGetEventName(teBtnCodeLockEvent eEvent);
#endif  // __BUTTON_CODE_LOCK_H__

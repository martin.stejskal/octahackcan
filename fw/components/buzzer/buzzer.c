/**
 * @file
 * @author Martin Stejskal
 * @brief Buzzer driver
 */
// ===============================| Includes |================================
// Standard
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

// ESP specific
#include <driver/dac.h>
#include <esp32/rom/ets_sys.h>  // Precise delays
#include <esp_log.h>
#include <esp_system.h>  // Random number generator

// Project specific
#include "buzzer.h"
#include "cfg.h"
#include "nvs_settings.h"
#include "ring_buff.h"
// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
typedef enum {
  NOTIFICATION_GENERIC,
  NITIFICATION_EXIT,
} teNotifyEventType;

typedef struct {
  // Queue as ring buffer
  tsRingBuffer sQueueRingBuffer;

  // Queue memory
  tsBuzzerBeepCfg sQueueMemory[BUZZER_QUEUE_SIZE_ITEMS];

  // Buzzer volume
  uint8_t u8volumeRaw;

  // Handler to buzzer task. Need for effective handling requests
  TaskHandle_t xTaskHandler;

  // Says if RTOS task is active or not
  bool bIsTaskRunnig;
} tsRuntime;
// ===========================| Global variables |============================
/**
 * @brief Tag for logger
 */
static const char *TAG = "Buzzer";

/**
 * @brief Runtime variable
 *
 * Keep all shared data under one roof
 */
tsRuntime msRuntime = {
    .sQueueRingBuffer.u8itemSizeBytes = sizeof(tsBuzzerBeepCfg),
    .sQueueRingBuffer.u16bufferSizeBytes =
        sizeof(tsBuzzerBeepCfg) * BUZZER_QUEUE_SIZE_ITEMS,
    .bIsTaskRunnig = false,
};
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
/**
 * @brief Initialize runtime structure
 * @param psRuntime Pointer to runtime structure
 */
static void _initRuntimeVariable(tsRuntime *psRuntime);

/**
 * @brief Play given sound
 * @param psQueueItem Pointer to item from queue
 */
static void _playSound(const tsBuzzerBeepCfg *psQueueItem);

/**
 * @brief Enable or disable buzzer
 * @param bEnable If true, enable buzzer. Otherwise it disable it.
 */
static void _enableBuzzer(bool bEnable);

/**
 * @brief Common exit routine for most use cases
 */
static void _exitRoutine(void);
// =========================| High level functions |==========================
void buzzerTaskRTOS(void *pvParameters) {
  // Task is technically running -> set variable as soon as possible
  msRuntime.bIsTaskRunnig = true;

  esp_err_t eErrCode = ESP_FAIL;

  eErrCode = dac_output_enable(BUZZER_DAC_PIN);
  eErrCode |= dac_output_voltage(BUZZER_DAC_PIN, 0);

  // Initialize HW
  if (eErrCode) {
    // This should not happen, but if so, we can keep live without buzzer
    ESP_LOGE(TAG, "Initialization failed (can not set DAC)");
    _exitRoutine();
    return;
  }

  // Need to setup few things in the structure
  _initRuntimeVariable(&msRuntime);

  // Pass event value - not actually used
  teNotifyEventType eEvent;

  // Current instance for item in queue
  tsBuzzerBeepCfg sQueueItem;

  for (;;) {
    // Wait until incoming request
    xTaskNotifyWait(0, 0, &eEvent, portMAX_DELAY);
    ESP_LOGI(TAG, "Woken up. Event: %d", eEvent);
    if (eEvent == NITIFICATION_EXIT) {
      // Request to exit task gracefully
      _exitRoutine();
      return;
    }
    // Else it is normal event - keep going

    // Load parameters from buffer
    ringBuffPull(&msRuntime.sQueueRingBuffer, &sQueueItem);

    // Sanitary check
    if (sQueueItem.u16timeBeepMs == 0) {
      ESP_LOGE(TAG, "Zero beep time loaded from ring buffer! R: %d W: %d",
               msRuntime.sQueueRingBuffer.u16readIdx,
               msRuntime.sQueueRingBuffer.u16writeIdx);

      // Only way how to deal with that is to reset ring buffer and hope it
      // helps. Playing old sounds does not sound like good idea
      ringBuffClean(&msRuntime.sQueueRingBuffer);

    } else {
      // Play sound
      _playSound(&sQueueItem);
    }
  }

  ESP_LOGE(TAG, "Buzzer task failed");

  _exitRoutine();
}

void buzzerTaskRequestExit(void) {
  // Setup variables
  BaseType_t xHigherPriorityTaskWoken = pdFALSE;

  // Notify request
  xTaskNotifyFromISR(msRuntime.xTaskHandler, NITIFICATION_EXIT,
                     eSetValueWithOverwrite, &xHigherPriorityTaskWoken);
}

bool buzzerTaskIsRunning(void) { return msRuntime.bIsTaskRunnig; }
// ========================| Middle level functions |=========================
esp_err_t buzzerSingleBeep(const uint16_t u16beepDurationMs) {
  // Value 0 is not allowed
  if (u16beepDurationMs == 0) {
    return ESP_ERR_INVALID_ARG;
  }

  // Setup variables
  BaseType_t xHigherPriorityTaskWoken = pdFALSE;

  // This is simple beep
  tsBuzzerBeepCfg sBeepParameters = {.u16timeBeepMs = u16beepDurationMs,
                                     .u16timeSilenceMs = 0,
                                     .u16repeatCount = 0};

  ///@todo Check for buffer overflow?

  // Push to ring buffer
  ringBuffPush(&msRuntime.sQueueRingBuffer, &sBeepParameters);

  // And notify task that there is another payload
  xTaskNotifyFromISR(msRuntime.xTaskHandler, NOTIFICATION_GENERIC, eNoAction,
                     &xHigherPriorityTaskWoken);

  return ESP_OK;
}

esp_err_t buzzerBeep(const tsBuzzerBeepCfg *psBeepCfg) {
  // Pointer should not be empty
  assert(psBeepCfg);

  // Value 0 is not allowed
  if (psBeepCfg->u16timeBeepMs == 0) {
    return ESP_ERR_INVALID_ARG;
  }

  // Setup variables
  BaseType_t xHigherPriorityTaskWoken = pdFALSE;

  ///@todo Check for buffer overflow?

  // Push to ring buffer
  ringBuffPush(&msRuntime.sQueueRingBuffer, psBeepCfg);

  // And notify task that there is another payload
  xTaskNotifyFromISR(msRuntime.xTaskHandler, NOTIFICATION_GENERIC, eNoAction,
                     &xHigherPriorityTaskWoken);

  return ESP_OK;
}
// ==========================| Low level functions |==========================
esp_err_t buzzerSetLevel(const uint8_t u8level) {
  return (dac_output_voltage(BUZZER_DAC_PIN, u8level));
}
// ==========================| Internal functions |===========================
static void _initRuntimeVariable(tsRuntime *psRuntime) {
  // Ring buffer related stuff. Need to set pointer to queue memory since this
  // can not be done during runtime variable initialization (I did not figure
  // out how).
  psRuntime->sQueueRingBuffer.pvBuffer = psRuntime->sQueueMemory;

  ringBuffClean(&psRuntime->sQueueRingBuffer);

  // Load default volume
  ///@todo Load it from NVM
  psRuntime->u8volumeRaw = -1;  // This set it to 0xFF

  // Get handler to current task. Needed for notifications (instead of using
  // delays)
  psRuntime->xTaskHandler = xTaskGetCurrentTaskHandle();
}

static void _playSound(const tsBuzzerBeepCfg *psQueueItem) {
  uint16_t u16repeatCount = 0;
  do {
    _enableBuzzer(true);

    if (psQueueItem->u16timeBeepMs <= 10) {
      // Super short delay
      ets_delay_us(psQueueItem->u16timeBeepMs * 1000);
    } else {
      // Regular RTOS delay
      vTaskDelay(psQueueItem->u16timeBeepMs / portTICK_PERIOD_MS);
    }

    _enableBuzzer(false);

    if (psQueueItem->u16timeSilenceMs == 0) {
      // just pass
    } else if (psQueueItem->u16timeSilenceMs <= 10) {
      // Super short delay
      ets_delay_us(psQueueItem->u16timeSilenceMs * 1000);
    } else {
      // Regular RTOS delay
      vTaskDelay(psQueueItem->u16timeSilenceMs / portTICK_PERIOD_MS);
    }

    u16repeatCount++;
  } while (u16repeatCount <= psQueueItem->u16repeatCount);
}

static void _enableBuzzer(bool bEnable) {
  if (bEnable) {
    buzzerSetLevel(msRuntime.u8volumeRaw);
  } else {
    buzzerSetLevel(0);
  }
}

static void _exitRoutine(void) {
  dac_output_disable(BUZZER_DAC_PIN);

  msRuntime.bIsTaskRunnig = false;

  // This have to be last, since it technically exit current RTOS function
  vTaskDelete(NULL);
}

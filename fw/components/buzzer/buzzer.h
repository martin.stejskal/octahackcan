/**
 * @file
 * @author Martin Stejskal
 * @brief Buzzer driver
 */
#ifndef __BUZZER_H__
#define __BUZZER_H__
// ===============================| Includes |================================
#include <esp_err.h>
#include <stdbool.h>
#include <stdint.h>
// ================================| Defines |================================
/**
 * @brief Queue size
 *
 * Number of requests which will be served.
 */
#define BUZZER_QUEUE_SIZE_ITEMS (10)
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
typedef struct {
  // Buzzer on for this period. Value is in ms
  uint16_t u16timeBeepMs;

  // Buzzer off for this period. Value is in ms
  uint16_t u16timeSilenceMs;

  // How many times this should be repeated
  uint16_t u16repeatCount;
} tsBuzzerBeepCfg;
// ===============================| Functions |===============================
// =========================| High level functions |==========================
/**
 * @brief Beep task for RTOS
 * @param pvParameters Pointer to parameters. Actually none are required.
 */
void buzzerTaskRTOS(void *pvParameters);

/**
 * @brief Send request to task to gracefully exit
 *
 * This will just send request. It is up to RTOS and priorities to let buzzer
 * task to do it's job.
 */
void buzzerTaskRequestExit(void);

/**
 * @brief Allows to check whether task is still running or not
 * @return True if still running, false otherwise.
 */
bool buzzerTaskIsRunning(void);
// ========================| Middle level functions |=========================
/**
 * @brief Simple beep sound
 * @param u16beepDurationMs Beep duration in ms
 * @return ESP_OK if everything went smoothly
 */
esp_err_t buzzerSingleBeep(const uint16_t u16beepDurationMs);

/**
 * @brief Complex beep
 *
 * Allow generate bit more sophisticated sound than buzzerSingleBeep()
 *
 * @param psBeepCfg Pointer to beep parameter structure
 * @return ESP_OK if everything went smoothly
 */
esp_err_t buzzerBeep(const tsBuzzerBeepCfg *psBeepCfg);
// ==========================| Low level functions |==========================
/**
 * @brief Set output level on buzzer pin
 *
 * @param u8level 0~255 where 0 means completely off and 255 is maximum volume
 * @return ESP_OK if no error
 */
esp_err_t buzzerSetLevel(const uint8_t u8level);
#endif  // __BUZZER_H__

/**
 * @file
 * @author Martin Stejskal
 * @brief Octa Hack CAN application
 *
 * Also take care about power saving.
 */
#ifndef __POWER_SAVE_H__
#define __POWER_SAVE_H__
// ===============================| Includes |================================
#include <stdbool.h>
// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
// =========================| High level functions |==========================
/**
 * @brief Task which handles GPIO readings and put device into sleep state
 *
 * @param pvParameters No parameters required
 */
void appTask(void *pvParameters);
// ========================| Middle level functions |=========================
/**
 * @brief Notify application layer that service mode is running on background
 *
 * When service mode is running, sleep mode is postponed until disabled.
 *
 * @param bServiceMode True if service mode is enabled, false otherwise
 */
void appSetServiceMode(bool bServiceMode);
// ==========================| Low level functions |==========================
// ==========================| Internal functions |===========================
#endif  // __POWER_SAVE_H__

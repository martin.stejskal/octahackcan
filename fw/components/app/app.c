/**
 * @file
 * @author Martin Stejskal
 * @brief Octa Hack CAN application
 *
 * Also take care about power saving.
 */
// ===============================| Includes |================================
#include <assert.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

// ESP specific
#include <driver/adc.h>
#include <driver/dac.h>
#include <driver/gpio.h>
#include <driver/rtc_io.h>
#include <driver/uart.h>
#include <esp_log.h>
#include <esp_sleep.h>

#include "app.h"
#include "button_code_lock.h"
#include "buzzer.h"
#include "can_app.h"
#include "cfg.h"
#include "led_rgb.h"
#include "power_manager.h"
#include "ui_service_core.h"
// ================================| Defines |================================
/**
 * @brief How many times will be read ADC value when estimating new threshold
 *
 * When getting value from potentiometer it is better to average samples, so
 * noise will be suppressed.
 */
#define _NUM_OF_ADC_READINGS_SET_THRESHOLD (100)

#define _EXIT_TASK_GRACEFULLY_NUM_OF_RETRY (5)
#define _EXIT_TASK_GRACEFULLY_WAIT_PERIOD_MS (20)
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// Note that GPIO 34 ~ 39 does not have pull-up resistor -> not suitable
#if BTN_CODE_LOCK_PIN > 33
#error "The BTN_CODE_LOCK_PIN have to be GPIO with pull-up feature"
#endif

#if NO_ACTIVITY_TIMEOUT_S > 10000
#error "Really? No activity set so high? Are you high?"
#endif

#if FACTORY_RESET_DURATION_S < SET_SENSITIVITY_THRESHOLD_S
#error "Factory reset duration have to be higher than other events"
#endif

#if FACTORY_RESET_DURATION_S >= NO_ACTIVITY_TIMEOUT_S
#error "Factory reset duration have to be lower than no activity timeout"
#endif

#if SET_SENSITIVITY_THRESHOLD_S >= NO_ACTIVITY_TIMEOUT_S
#error "Sensitivity threshold have to be lower than no activity timeout"
#endif

// ===========================| Structures, enums |===========================
typedef struct {
  /**
   * @brief Keep info about last button activity
   *
   * This information is required in order to properly decide when is time to
   * put device into deep sleep mode.
   */
  uint32_t u32lastActivityMs;

  /**
   * @brief Track when "factory reset" button was pressed
   */
  uint32_t u32factoryResetPressedMs;

  /**
   * @brief Set when service mode is enabled
   *
   * Set via appSetServiceMode() function
   */
  bool bServiceMode;
} tsRuntimeApp;
// ===========================| Global variables |============================
/**
 * @brief Tag for logger
 */
static const char *TAG = "App";

/**
 * @brief Keep all runtime things at one place
 */
static tsRuntimeApp msRuntime;
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
/**
 * @brief Initialize GPIO
 *
 * Waits until code lock button is in high level (default)
 */
static void _initGpio(void);

/**
 * @brief Called when pin level changes on CAN activate
 * @param iGpioLevel Current level on pin
 */
static void _canActiveLevelChange(const int iGpioLevel);

/**
 * @brief Called when pin level changes on Code lock
 * @param iGpioLevel Current level on pin
 */
static void _codeLockLevelChange(const int iGpioLevel);

/**
 * @brief Deal with "reset button" events
 *
 * The "reset button" can be multi-purpose button. Main feature is factory
 * reset of course.
 *
 * @param iIoFactoryReset Status on "reset button". Non-zero value means
 *        "button pressed"
 * @param iIoCanActive Status of CAN bus. Zero value means "CAN inactive",
 *        "CAN active" otherwise
 * @param psRuntime Pointer to structure with runtime variables
 */
static void _handleFactoryResetButton(const int iIoFactoryReset,
                                      const int iIoCanActive,
                                      tsRuntimeApp *psRuntime);

/**
 * @brief Restore factory settings and reboot device
 *
 * Rebooting is the cleanest way to make sure that all setting is reloaded.
 */
static void _restoreFactorySettingsRebootDevice(void);

/**
 * @brief Set new code lock threshold
 *
 * Device is not required to reset. This is "on the fly" variable.
 */
static void _setNewCodeLockThreshold(void);

/**
 * @brief Put device into deep sleep mode
 *
 * Disable all current wake up settings, configure RTC wake up sources and
 * enable deep sleep mode
 */
static void _deep_sleep_mode(void);

/**
 * @brief Try to close all tasks gracefully
 *
 * @return ESP_OK if everything is fine, something else otherwise
 */
static esp_err_t _exitAllTasksGracefully(void);

/**
 * @brief Before going to deep sleep, disable all unused peripherals
 *
 * Disabling unused peripherals is important step just before going to deep
 * sleep mode. Otherwise ESP core will consume some energy for nothing.
 */
static void _disableAllPeripherals(void);

/**
 * @brief Enable LED which signalize device activity
 *
 * @note This works only when NOT in debug mode. In debug note it would be
 *       pointless to show up that device is active, so GPIO was reused for
 *       this purpose only when debug is not active
 */
static void _enablePowerOnLed(void);
// =========================| High level functions |==========================
void appTask(void *pvParameters) {
  // Very first thing in user space
  _enablePowerOnLed();

  // Keep GPIO values
  int iIoCanActiveCurrent, iIoCanActivePrevious, iIoCodeLockCurrent,
      iIoCodeLockPrevious, iIoFactoryReset;

  // Set to true if some button was pressed
  bool bAtLeastOneChange = false;

  // Configuration for CAN application
  ts_can_app_args s_can_cfg = {.pf_enable_transciever_cb =
                                   pm_set_can_transceiver};

  // Initialize buttons & RGB LED
  _initGpio();

  // Load initial button values
  iIoCodeLockPrevious = gpio_get_level(BTN_CODE_LOCK_PIN);
  iIoCanActivePrevious = gpio_get_level(CAN_ACTIVE_PIN);
  iIoFactoryReset = gpio_get_level(FACTORY_RESET_PIN);

  ESP_LOGI(TAG,
           "Initial GPIO status:\n Code Lock: %d | CAN activity: %d | Factory "
           "reset: %d",
           iIoCodeLockPrevious, iIoCanActivePrevious, iIoFactoryReset);

  // Need to update "last activity" in order to avoid immediate deep sleep
  msRuntime.u32lastActivityMs = GET_CLK_MS_32BIT();

  // Reset time for "factory reset" press
  msRuntime.u32factoryResetPressedMs = 0;

  // Setup also code lock process
  buttonCodeLockInitialize();

  // Enable +5V power supply
  pm_set_5v_power_supply(true);

  // Initialize CAN application module
  ESP_ERROR_CHECK(can_app_init(&s_can_cfg));

  while (1) {
    iIoCodeLockCurrent = gpio_get_level(BTN_CODE_LOCK_PIN);
    iIoCanActiveCurrent = gpio_get_level(CAN_ACTIVE_PIN);
    iIoFactoryReset = gpio_get_level(FACTORY_RESET_PIN);

    // Clean up
    bAtLeastOneChange = false;

    // The both IO are active in low -> need to invert logic a bit. Also it
    // make sense only report changes
    if (iIoCanActivePrevious != iIoCanActiveCurrent) {
      // Change -> do something
      ESP_LOGD(TAG, "CAN active detection. Level change to: %d",
               iIoCanActiveCurrent);

      _canActiveLevelChange(iIoCanActiveCurrent);
      bAtLeastOneChange = true;
    }

    if (iIoCodeLockPrevious != iIoCodeLockCurrent) {
      // Change -> do something
      ESP_LOGD(TAG, "Code lock. Level change to: %d", iIoCodeLockCurrent);

      _codeLockLevelChange(iIoCodeLockCurrent);
      bAtLeastOneChange = true;
    }

    // Process also reset button. This might include other features
    _handleFactoryResetButton(iIoFactoryReset, iIoCanActiveCurrent, &msRuntime);

    // Need to keep track about activity. Also service mode prevent system
    // from default sleep method
    if (bAtLeastOneChange || msRuntime.bServiceMode) {
      msRuntime.u32lastActivityMs = GET_CLK_MS_32BIT();
    } else {
      // No activity -> check if there is time for deep sleep.
      if ((GET_CLK_MS_32BIT() - msRuntime.u32lastActivityMs) >
          (NO_ACTIVITY_TIMEOUT_S * 1000)) {
        // Do not go to deep sleep if CAN is active (car is "wake up"), but
        // at least restart "code lock" state machine
        if (iIoCanActiveCurrent) {
          buttonCodeLockInitialize();

          // Also need to reset "last" activity, in order to not fall into this
          // condition all the time
          msRuntime.u32lastActivityMs = GET_CLK_MS_32BIT();

        } else {
          // Can is inactive and there is no activity from buttons
          _deep_sleep_mode();
        }
      }
    }

    iIoCanActivePrevious = iIoCanActiveCurrent;
    iIoCodeLockPrevious = iIoCodeLockCurrent;

    // Give RTOS time time to process also other tasks
    taskYIELD();
  }
}
// ========================| Middle level functions |=========================
void appSetServiceMode(bool bServiceMode) {
  if (bServiceMode) {
    ESP_LOGI(TAG, "Service mode enabled");
  } else {
    ESP_LOGI(TAG, "Service mode disabled");
  }

  msRuntime.bServiceMode = bServiceMode;
}
// ==========================| Low level functions |==========================
// ==========================| Internal functions |===========================
static void _initGpio(void) {
  // Note: Using macro ESP_ERROR_CHECK since we does not expect that any of the
  // step below fails. And if so, it is serious issue, which can not be
  // properly recovered anyways

  // Note: Can not use interrupt mode otherwise there will be called abort()
  // and whole system crash

  // ================================| Buttons |==============================

  // Initial configuration for code lock and factory reset button (same
  // configuration)
  gpio_config_t sGpioCfg = {
      .pin_bit_mask = (1ULL << BTN_CODE_LOCK_PIN) | (1ULL << FACTORY_RESET_PIN),
      .mode = GPIO_MODE_INPUT,
      .pull_up_en = 1,
      .pull_down_en = 0,
      .intr_type = GPIO_INTR_DISABLE};

  // Setup GPIO and check if failed or not
  ESP_ERROR_CHECK(gpio_config(&sGpioCfg));

  // Reconfigure structure for CAN activation detector
  sGpioCfg.pin_bit_mask = (1ULL << CAN_ACTIVE_PIN);
  sGpioCfg.pull_up_en = 0;

  ESP_ERROR_CHECK(gpio_config(&sGpioCfg));

  // ================================| RGB LED |==============================
  // Turn off RGB LED - by default outputs are in low. And low level actually
  // enables LED.
  ESP_ERROR_CHECK(gpio_set_level(LED_G_PIN, 1));
  ESP_ERROR_CHECK(gpio_set_level(LED_B_PIN, 1));

  sGpioCfg.pin_bit_mask =
      (1ULL << LED_R_PIN) | (1ULL << LED_G_PIN) | (1ULL << LED_B_PIN);

  // LED are enabled by low level -> open drain is actually optimized way to go
  sGpioCfg.mode = GPIO_MODE_OUTPUT_OD;

  // Do not really want any pull-up/down
  sGpioCfg.pull_up_en = 0;
  ESP_ERROR_CHECK(gpio_config(&sGpioCfg));

  // Let red LED blink for very brief moment
  ESP_ERROR_CHECK(gpio_set_level(LED_R_PIN, 1));

  // ========================| Wait for expected level |======================
  ESP_LOGD(TAG, "Waiting for code lock button to get into high level");

  // Wait for high level for code lock button. No need for CAN activation
  // detector, since there was no pull-up, pull-down or anything. It is just
  // wired to the CAN bus all the time, so no changes on pin expected
  while (gpio_get_level(BTN_CODE_LOCK_PIN) == 0) {
    // Do not use delay, but instead use sleep mode
    // 10 ms sleep
    ESP_ERROR_CHECK(esp_sleep_enable_timer_wakeup(10000));
    ESP_ERROR_CHECK(esp_light_sleep_start());
  }

  ESP_LOGD(TAG, "Code lock button in high -> initialization done");
}

static void _canActiveLevelChange(const int iGpioLevel) {
  ///@todo Call some function which will deal with that
}
static void _codeLockLevelChange(const int iGpioLevel) {
  // Logic is reversed. Pressed button is in low level
  if (iGpioLevel) {
    // High -> released
    buttonCodeLockLevelChangeCallback(BTN_CODE_LOCK_RELEASED);
  } else {
    // Pressed
    buttonCodeLockLevelChangeCallback(BTN_CODE_LOCK_PRESSED);
  }
}

static void _handleFactoryResetButton(const int iIoFactoryReset,
                                      const int iIoCanActive,
                                      tsRuntimeApp *psRuntime) {
  // If CAN is not active, do not allow to reset factory settings or any other
  // action. Because when CAN is not active, car is locked -> we want to avoid
  // even not intended when car is "off"
  if (!iIoCanActive) {
    if (!iIoFactoryReset) {
      // Button is pressed -> at least show warning
      ESP_LOGW(TAG, "CAN is inactive. Can not reset factory settings");
    }

    // Reset counter for pressed button at any button state (ignore button
    // status)
    msRuntime.u32factoryResetPressedMs = 0;
    return;
  }

  if (iIoFactoryReset) {
    // Factory reset button is released -> check if there is another request.
    // Then reset registered time when it was pressed.
    // Process only if button was pressed previously
    if (msRuntime.u32factoryResetPressedMs) {
      if ((GET_CLK_MS_32BIT() - msRuntime.u32factoryResetPressedMs) >=
          (SET_SENSITIVITY_THRESHOLD_S * 1000)) {
        // Set new sensitivity threshold
        _setNewCodeLockThreshold();
      }
    }

    msRuntime.u32factoryResetPressedMs = 0;

  } else {
    // Factory reset button is pressed

    // Check how long is pressed
    if (msRuntime.u32factoryResetPressedMs) {
      // Button pressed for some time -> check how long is pressed
      if ((GET_CLK_MS_32BIT() - msRuntime.u32factoryResetPressedMs) >=
          (FACTORY_RESET_DURATION_S * 1000)) {
        _restoreFactorySettingsRebootDevice();
      } else {
        // Button should be pressed bit longer
      }

    } else {
      // Factory reset button pressed for the first time
      msRuntime.u32factoryResetPressedMs = GET_CLK_MS_32BIT();
    }
  }  // Button is pressed
}

static void _restoreFactorySettingsRebootDevice(void) {
  // Time to recover factory settings and reboot device
  ESP_LOGI(TAG, "Restoring factory reset...");

  // Mix white color
  ledTurnOnAll();

  // Send request to buzzer task. Keep buzzer on until device reboots
  buzzerSingleBeep(BEEP_FACTORY_RESET_MS);

  // Erase settings
  buttonCodeLockRestoreDefaults();

  // Let buzzer to do it's job
  vTaskDelay(pdMS_TO_TICKS(FACTORY_RESET_BEEP_DURATION_MS));

  // Reboot device. This will reload all settings.
  esp_restart();
}

static void _setNewCodeLockThreshold(void) {
  ESP_LOGI(TAG, "Setting new tolerance threshold for code lock");

  // Load current RGB settings -> restore it later on
  teLedRGBchannels sColor;
  ledRGBget(&sColor);

  // Mix blue-green color
  ledRGBsetRlvl(LED_LVL_MIN);
  ledRGBsetGlvl(LED_LVL_MAX);
  ledRGBsetBlvl(LED_LVL_MAX);

  // Signalize this action to user somehow
  // Make sound. Do not care about error code
  tsBuzzerBeepCfg sBuzzerCfg = BEEP_NEW_CODE_LOCK_THRESHOLD;
  buzzerBeep(&sBuzzerCfg);

  // Setup ADC. Range 0.1 ~ 3.3V will be used -> need to set also attenuation
  ESP_ERROR_CHECK(
      adc2_config_channel_atten(TOLERANCE_THRESHOLD_PIN, ADC_ATTEN_DB_11));

  // Setup pull-down. Need to be done after ADC initialization, otherwise ADC
  // functions will overwrite GPIO settings. Get GPIO number from ADC channel
  gpio_num_t iGpioNum;

  ESP_ERROR_CHECK(adc2_pad_get_io_num(TOLERANCE_THRESHOLD_PIN, &iGpioNum));

  ESP_ERROR_CHECK(gpio_pulldown_en(iGpioNum));

  // OK, so ADC should be ready. Now read out raw ADC value
  uint32_t u32adcRawSum = 0;
  int iAdcRaw;

  for (uint8_t u8readingNum = 0;
       u8readingNum < _NUM_OF_ADC_READINGS_SET_THRESHOLD; u8readingNum++) {
    ESP_ERROR_CHECK(
        adc2_get_raw(TOLERANCE_THRESHOLD_PIN, ADC_WIDTH_BIT_10, &iAdcRaw));

    // Do not expect negative number
    assert(iAdcRaw >= 0);
    u32adcRawSum += (uint32_t)iAdcRaw;
  }

  // Disable pull down in order to save power
  ESP_ERROR_CHECK(gpio_pulldown_dis(iGpioNum));

  // Calculate average raw ADC value
  u32adcRawSum /= _NUM_OF_ADC_READINGS_SET_THRESHOLD;

  // Following calculation depending on used HW. Calculation assume 45 kOhm
  // pull down enabled on ADC pin and 0~250 kOhm variable resistance connected
  // to 3.3 V. With attenuation above we expect on ADC values from 140 to 917.
  // These are ideal values, but just to give an idea. This range have to be
  // converted to range 0~255 due to code lock function.
  // Final formula is:
  // ThVal = adcRaw * 0.328 - 45.946
  //
  // But, using float is not good idea in embedded word. So the values
  // will be multiplied by 1000. As last step, there will be division by 1000
  int32_t i32thVal = u32adcRawSum * 328 - 45946;
  i32thVal /= 1000;

  // Make sure that value fits to 8 bit value
  if (i32thVal > 255) {
    i32thVal = 255;
  } else if (i32thVal < 0) {
    i32thVal = 0;
  }

  ESP_LOGD(TAG,
           "Set code lock threshold. Raw ADC: %d (10 bit), calc: %d (8 bit)",
           u32adcRawSum, i32thVal);

  // Set new threshold
  buttonCodeLockSetCommandTolerance(i32thVal);

  // Restore RGB PWM value. Delay to keep LED on some time
  vTaskDelay(LED_NEW_CODE_LOCK_THRESHOLD_MS / portTICK_PERIOD_MS);
  ledRGBset(sColor);
}

static void _deep_sleep_mode(void) {
#if HW_DEBUG_ON
  // If HW debug is on, disable deep sleep
  return;
#endif

  ESP_LOGI(TAG, "No activity for %d seconds. Going to the deep sleep...",
           NO_ACTIVITY_TIMEOUT_S);
  // Quitting tasks gracefully
  esp_err_t eErrCodeTasksExit = _exitAllTasksGracefully();
  if (eErrCodeTasksExit) {
    // If task does not exit gracefully, it is still chance that RTOS will
    // call it and if we would disable some peripheral which this task would
    // use, it could cause crash and halting core leads to much higher power
    // consumption. So in order to reduce this risk, we're trying to reach
    // point of deep sleep although may not all of the peripherals are
    // disabled. Last thing what can be done it to report this error
    ESP_LOGE(TAG,
             "FAILED to exit some tasks! This may have bad impact to "
             "power consumption!!!");
  }

  // Let UART finish it's job
  uart_wait_tx_idle_polling(CONFIG_ESP_CONSOLE_UART_NUM);

  // Disable all remaining peripherals - just to make sure
  _disableAllPeripherals();

  // Disable all current wake up events
  ESP_ERROR_CHECK(esp_sleep_disable_wakeup_source(ESP_SLEEP_WAKEUP_ALL));

  ESP_ERROR_CHECK(rtc_gpio_pullup_en(BTN_CODE_LOCK_PIN));

  // The ext0 can handle only 1 pin at any state (low, high)
  ESP_ERROR_CHECK(esp_sleep_enable_ext0_wakeup(BTN_CODE_LOCK_PIN, 0));
  // The ext1 can trigger only when ALL sources in low OR any of the source
  // is in high.
  ESP_ERROR_CHECK(esp_sleep_enable_ext1_wakeup((1ULL << CAN_ACTIVE_PIN),
                                               ESP_EXT1_WAKEUP_ANY_HIGH));

  if (esp_cpu_in_ocd_debug_mode()) {
    ESP_LOGW(TAG,
             "In debug mode. Will not going to sleep mode. That would break "
             "debug session!");
    return;
  }
  esp_deep_sleep_start();
}

static esp_err_t _exitAllTasksGracefully(void) {
  esp_err_t eErrCode = ESP_ERR_TIMEOUT;

  // Send requests to all related tasks (obviously not to self)
  buzzerTaskRequestExit();
  ledRGBtaskRequestExit();
  uiServiceTaskRequestExit();

  // Collect information if task is running or not
  bool bIsRunning;

  for (uint8_t u8numOfTry = 0; u8numOfTry < _EXIT_TASK_GRACEFULLY_NUM_OF_RETRY;
       u8numOfTry++) {
    // Small delay to give other tasks chance to quit
    vTaskDelay(pdMS_TO_TICKS(_EXIT_TASK_GRACEFULLY_WAIT_PERIOD_MS));

    // Load status from every task
    bIsRunning = buzzerTaskIsRunning();
    bIsRunning &= ledRGBtaskIsRunning();
    bIsRunning &= uiServiceTaskIsRunning();

    if (!bIsRunning) {
      // All tasks exited gracefully -> OK
      eErrCode = ESP_OK;
      break;
    }
  }  // Keep trying to exit tasks

  return eErrCode;
}

static void _disableAllPeripherals(void) {
  // Not really RTOS task - disable separately
  can_app_denit();

  // Disable all external modules
  pm_set_5v_power_supply(false);
  pm_set_can_transceiver(false);

  // GPIO. This is more delighted job. There available function
  // gpio_reset_pin(), which might sounds promising, but it enables pull-up
  // and therefore it depends, but in general it increase current consumption
  gpio_config_t sCfg = {
      .pin_bit_mask = BIT64(PWR_5V_EN_PIN) | BIT64(LED_POWER_ON_PIN) |
                      BIT64(CAN_ACTIVE_PIN) | BIT64(CAN_TRNSCVR_PWR_PIN) |
                      BIT64(FACTORY_RESET_PIN),
      .mode = GPIO_MODE_DISABLE,
      .pull_up_en = false,  // Need to be disabled
      .pull_down_en = false,
      .intr_type = GPIO_INTR_DISABLE,
  };
  gpio_config(&sCfg);
}

static void _enablePowerOnLed(void) {
  // Setup GPIO
  gpio_config_t sCfg = {
      .pin_bit_mask = BIT64(LED_POWER_ON_PIN),
      .mode = GPIO_MODE_OUTPUT,
      .pull_up_en = false,
      .pull_down_en = false,
      .intr_type = GPIO_INTR_DISABLE,
  };
  gpio_config(&sCfg);

  // Turn on LED
  gpio_set_level(LED_POWER_ON_PIN, 1);
}

/**
 * @file
 * @author Martin Stejskal
 * @brief Configuration file for UI core
 */
#ifndef __UI_SERVICE_CFG_H__
#define __UI_SERVICE_CFG_H__
// ===============================| Includes |================================
// ================================| Defines |================================
#define UI_SERVICE_GET_PWD() uiServiceUserGetPassword()
#define UI_SERVICE_RESTORE_FACTORY_SETTINGS() \
  uiServiceUserRestoreFactorySettings()

#define UI_SERVICE_GET_PTR_TO_CMD_LIST() uiServiceUserGetListOfCmds()
#define UI_SERVICE_GET_NUM_OF_CMDS() uiServiceUserGetNumOfCmds()

#define UI_SERVICE_ENABLE_SERVICE_MODE() uiServiceUserSetServiceMode(1)
#define UI_SERVICE_DISABLE_SERVICE_MODE() uiServiceUserSetServiceMode(0)
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =========================| High level functions |==========================
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
#endif  // __UI_SERVICE_CFG_H__

/**
 * @file
 * @author Martin Stejskal
 * @brief User/project specific module for UI service code
 */
// ===============================| Includes |================================

#include "ui_service_user.h"

#include <esp_log.h>
#include <string.h>

#include "app.h"
#include "button_code_lock.h"
#include "can_app.h"
#include "cfg.h"
#include "nvs_settings.h"
#include "ui_service_core.h"
#include "ui_service_utils.h"
// ================================| Defines |================================
/**
 * @brief Default password
 *
 * This password will be used until user does not define own password
 */
#define _DEFAULT_PASSWORD ("octahack")

/**
 * @brief Define maximum password length in characters
 *
 * Be honest, who is using password longer than 20 characters?
 */
#define _PASSWORD_MAX_LEN (50)
/**
 * @brief List of commands used in this module
 *
 * Sometimes is needed to know how command itself (without argument) is long.
 * So here is it stored at one place.
 *
 * @{
 */
#define _CMD_CAN_RX "can rx"
#define _CMD_CAN_TX "can tx"
#define _CMD_PASSWD "passwd"
/**
 * @}
 */

#define _EXIT_IF_ERR(function) \
  eErrCode = function;         \
  if (eErrCode) {              \
    return eErrCode;           \
  }

/**
 * @brief Get size of item in structure
 */
#define _ITEM_SIZE(structure, item) sizeof(((structure*)0)->item)
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
/**
 * @brief Tag for logger
 */
static const char* TAG = "UI service (u)";

/**
 * @brief String used for NVS to look up for password
 */
static const char* mpacPwdNvs = "UI user pwd";

static const tsUiServiceCmdCfg masUserCmds[] = {
    {
        .pacCommand = _CMD_CAN_RX,
        .pacHelp = "Scan CAN bus until stopped pressing ESC key. Format is "
                   "[timestamp] [ID] [Bytes]",
        .bRequireArgument = false,
        .pfCmdCallback = uiServiceCmdCanRx,
    },
    {.pacCommand = _CMD_CAN_TX,
     .pacHelp = "Send data over CAN bus. Format is [ID] [Bytes]. All in hex.",
     .bRequireArgument = true,
     .pfCmdCallback = uiServiceCmdCanTx,

     .sArgCfg = {.eArgType = UI_SRVCE_CMD_ARG_TYPE_STR,
                 .pacArgumentExample = "291 8A0001"}},
    {.pacCommand = "can lock",
     .pacHelp = "TX lock sequence",
     .bRequireArgument = false,
     .pfCmdCallback = uiServiceCmdLock,

     .sArgCfg = {.eArgType = 0, .pacArgumentExample = ""}},
    {.pacCommand = "can unlock",
     .pacHelp = "TX unlock sequence",
     .bRequireArgument = false,
     .pfCmdCallback = uiServiceCmdUnlock,

     .sArgCfg = {.eArgType = 0, .pacArgumentExample = ""}},
    {.pacCommand = _CMD_PASSWD,
     .pacHelp = "Set new password",
     .bRequireArgument = true,
     .pfCmdCallback = uiServiceCmdSetPwwd,

     .sArgCfg =
         {
             .eArgType = UI_SRVCE_CMD_ARG_TYPE_STR,
             .pacArgumentExample = "hAck3r",
         }},

    {.pacCommand = "get sensitivity",
     .pacHelp = "Get sensitivity threshold for code lock. Range is "
                "from 0 (strict) to 255 (laid back)",
     .bRequireArgument = false,
     .pfCmdCallback = uiServiceCmdGetSensitivity,

     .sArgCfg = {.eArgType = 0, .pacArgumentExample = ""}},

    {.pacCommand = "set sensitivity",
     .pacHelp = "Set sensitivity threshold for code lock. Range is "
                "from 0 (strict) to 255 (laid back)",
     .bRequireArgument = true,
     .pfCmdCallback = uiServiceCmdSetSensitivity,

     .sArgCfg = {.eArgType = UI_SRVCE_CMD_ARG_TYPE_INT,
                 .pacArgumentExample = "70",
                 .i32min = 0,
                 .i32max = 255}},

};
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
// =========================| High level functions |==========================
esp_err_t uiServiceUserRestoreFactorySettings(void) {
  ESP_LOGI(TAG, "Restoring factory settings...");

  buttonCodeLockRestoreDefaults();
  return ESP_OK;
}
// ========================| Middle level functions |=========================
const char* uiServiceUserGetPassword(void) {
  static char acPwd[_PASSWORD_MAX_LEN];

  // Store default password somewhere
  const char* pacDefaultPwd = _DEFAULT_PASSWORD;

  if (nvsSettingsGet(mpacPwdNvs, acPwd, pacDefaultPwd, _PASSWORD_MAX_LEN)) {
    // If something went wrong, not much to do. Report error and use default
    uiServicePrintStr("Reading password failed. Using default");

    memcpy(acPwd, pacDefaultPwd, strlen(pacDefaultPwd));
  }

  return acPwd;
}

inline void uiServiceUserSetServiceMode(bool bServiceMode) {
  appSetServiceMode(bServiceMode);
}
// ==========================| Low level functions |==========================
const tsUiServiceCmdCfg* uiServiceUserGetListOfCmds(void) {
  return masUserCmds;
}

uint16_t uiServiceUserGetNumOfCmds(void) {
  return (sizeof(masUserCmds) / sizeof(masUserCmds[0]));
}

// ===============================| Commands |================================
void uiServiceCmdCanRx(tsUiServiceCbArgs* psCbArgs) {
  ESP_LOGI(TAG, "Enabling CAN RX mode...");

  // Predefine necessary informations
  ts_can_app_payload sCanPayload = {
      .u16_delay_after_ms = 0,
      // There should be at least some timeout to let task processing. If
      // nothing will appear on CAN bus, it can check if user pressed some
      // button
      .u32_operation_timeout_ms = 100,
  };

  esp_err_t eErrCode = ESP_FAIL;

  // Capture some special keys. It allow to quit RX mode
  teUiServiceSpecialKey eSpecialKey = KEY_NONE;

  // Need temporary buffer for received Bytes
  char acRxBytes[] = "FF FF FF FF FF FF FF FF";

  // Relatively speaking, "infinite loop". Waiting for user input actually
  while (1) {
    eErrCode = can_app_rx(&sCanPayload, 1);

    if (eErrCode == ESP_ERR_TIMEOUT) {
      // This is expected, do nothing
    } else if (eErrCode == 0) {
      // Convert received data to string
      eErrCode = usuBytesToStrHex(sCanPayload.u8_data, sCanPayload.u8_data_len,
                                  ' ', acRxBytes, sizeof(acRxBytes));
      if (eErrCode) {
        uiServicePrintf("Converting to data failed: %s\n",
                        esp_err_to_name(eErrCode));
      } else {
        // Print payload with timestamp
        uiServicePrintf("%d %x %s\n", GET_CLK_MS_32BIT(), sCanPayload.u32_id,
                        acRxBytes);
      }
    } else {
      // Some other error. Print error at least
      ESP_LOGE(TAG, "CAN rx: Err: %d", eErrCode);
    }

    // Check if user want to continue sniffing CAN or if want to exit
    eSpecialKey = uiServiceCheckForEnterEsc(psCbArgs->pacInputString);
    if (eSpecialKey == KEY_ESC) {
      uiServicePrintStr("Exiting RX mode");
      return;
    }
  }
}
void uiServiceCmdCanTx(tsUiServiceCbArgs* psCbArgs) {
  ESP_LOGD(TAG, "Transmitting data (%s) over CAN...", psCbArgs->pacArgs);

  // Store error code. Just in case
  esp_err_t eErrCode = ESP_FAIL;

  // Store ID
  uint32_t u32_id;

  eErrCode = usuStrHexToUint32(psCbArgs->pacArgs, &u32_id);

  // If something went wrong, nothing to do. Only tell user
  if (eErrCode) {
    uiServicePrintStr("Can not parse ID argument. Expected hexadecimal value.");
    return;
  }

  // Now it is time for payload
  uint8_t au8payload[_ITEM_SIZE(ts_can_app_payload, u8_data)];
  uint16_t u16payloadLength;

  // Move pointer to next "word". Do not forget that between arguments should
  // be spaces -> +1
  psCbArgs->pacArgs += 1 + usuGetWordLength(psCbArgs->pacArgs);

  // Now at "args" should be data
  eErrCode = usuStrHexToBytes(psCbArgs->pacArgs, sizeof(au8payload), au8payload,
                              &u16payloadLength);
  switch (eErrCode) {
    case ESP_ERR_NOT_FOUND:
      uiServicePrintStr("Data not defined. Please define also payload.");
      return;
    case ESP_ERR_NO_MEM:
      uiServicePrintStr("Too long payload. Please shorten payload.");
      return;
    case ESP_OK:
      break;
    default:
      uiServicePrintStr(
          "Can not parse payload data. Expected hexadecimal values.");
      return;
  }

  if (u16payloadLength > _ITEM_SIZE(ts_can_app_payload, u8_data)) {
    uiServicePrintStr("Too long payload. Can not transmit so many Bytes");
    return;
  }

  // Generate debug message. For every Byte in payload we need 2 ASCII
  // characters + NULL (1 character)
  char acDataDebug[(_ITEM_SIZE(ts_can_app_payload, u8_data) * 2) + 1] = {0};

  // Convert Bytes to string. Do not use any separator character
  eErrCode = usuBytesToStrHex(au8payload, u16payloadLength, '\0', acDataDebug,
                              sizeof(acDataDebug));

  ESP_LOGD(TAG, "ID: %d ; Data: %s", u32_id, acDataDebug);

  // Send over CAN
  ts_can_app_payload sCanPayload = {
      .u32_id = u32_id,
      .u8_data_len = u16payloadLength,
      .u16_delay_after_ms = 0,
      .u32_operation_timeout_ms = UI_SERVICE_USER_CAN_TIMEOUT_MS};
  memcpy(sCanPayload.u8_data, au8payload, sizeof(sCanPayload.u8_data));

  // And transmit it
  eErrCode =
      can_app_tx(&sCanPayload, sizeof(sCanPayload) / sizeof(ts_can_app_payload));

  if (eErrCode) {
    // Some error detected. Print also reason
    char acFailMsg[] = "[FAIL] E: 123456789";
    // Start from 10th character (after E: )
    sprintf(&acFailMsg[10], "%d", eErrCode);
    uiServicePrintStr(acFailMsg);
  } else {
    // Print OK
    uiServicePrintStr(uiServiceTxtOk());
  }
}

void uiServiceCmdLock(tsUiServiceCbArgs* psCbArgs) {
  ESP_LOGI(TAG, "Locking");
  esp_err_t eErrCode = can_app_lock();

  if (eErrCode) {
    uiServicePrintStr(uiServiceTxtFail());
  } else {
    uiServicePrintStr(uiServiceTxtOk());
  }
}

void uiServiceCmdUnlock(tsUiServiceCbArgs* psCbArgs) {
  ESP_LOGI(TAG, "Unlocking");
  esp_err_t eErrCode = can_app_unlock();

  if (eErrCode) {
    uiServicePrintStr(uiServiceTxtFail());
  } else {
    uiServicePrintStr(uiServiceTxtOk());
  }
}

void uiServiceCmdSetPwwd(tsUiServiceCbArgs* psCbArgs) {
  if (strlen(psCbArgs->pacArgs) >= _PASSWORD_MAX_LEN) {
    uiServicePrintStr("[FAIL] New password is too long");
    return;
  }

  if (nvsSettingsSet(mpacPwdNvs, psCbArgs->pacArgs, _PASSWORD_MAX_LEN)) {
    // Something went wrong
    uiServicePrintStr("[FAIL] Saving password failed");
  } else {
    uiServicePrintStr(uiServiceTxtOk());
  }
}

void uiServiceCmdGetSensitivity(tsUiServiceCbArgs* psCbArgs) {
  // Raw read value from API
  volatile static uint8_t u8val;
  u8val = buttonCodeLockGetCommandTolerance();

  uiServicePrintf("%d\n", u8val);
}
void uiServiceCmdSetSensitivity(tsUiServiceCbArgs* psCbArgs) {
  assert(psCbArgs->uValue.u32value <= 0xFF);

  buttonCodeLockSetCommandTolerance(psCbArgs->uValue.u32value);
  uiServicePrintStr(uiServiceTxtOk());
}

// ==========================| Internal functions |===========================

/**
 * @file
 * @author Martin Stejskal
 * @brief User/project specific module for UI service code
 */
#ifndef __UI_SERVICE_USER_H__
#define __UI_SERVICE_USER_H__
// ===============================| Includes |================================
#include <esp_err.h>

#include "ui_service_core.h"
// ================================| Defines |================================
#define UI_SERVICE_USER_CAN_TIMEOUT_MS (2000)
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =========================| High level functions |==========================
esp_err_t uiServiceUserRestoreFactorySettings(void);

// ========================| Middle level functions |=========================
const char *uiServiceUserGetPassword(void);

void uiServiceUserSetServiceMode(bool bServiceMode);
// ==========================| Low level functions |==========================
const tsUiServiceCmdCfg *uiServiceUserGetListOfCmds(void);
uint16_t uiServiceUserGetNumOfCmds(void);

// ===============================| Commands |================================
void uiServiceCmdCanRx(tsUiServiceCbArgs *psCbArgs);
void uiServiceCmdCanTx(tsUiServiceCbArgs *psCbArgs);
void uiServiceCmdLock(tsUiServiceCbArgs *psCbArgs);
void uiServiceCmdUnlock(tsUiServiceCbArgs *psCbArgs);
void uiServiceCmdSetPwwd(tsUiServiceCbArgs *psCbArgs);
void uiServiceCmdGetSensitivity(tsUiServiceCbArgs *psCbArgs);
void uiServiceCmdSetSensitivity(tsUiServiceCbArgs *psCbArgs);

#endif  // __UI_SERVICE_USER_H__

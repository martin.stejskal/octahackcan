/**
 * @file
 * @author Martin Stejskal
 * @brief Driver for RGB LED
 */

#ifndef __LED_RGB_H__
#define __LED_RGB_H__
// ===============================| Includes |================================
#include <stdint.h>
// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
/**
 * @brief LED level as 0/10 value
 *
 * Sometimes is range 0~255 too grained, so more rough steps are necessary.
 * Problem is that human eye is not linear, so levels can not be simply
 * 255/number of values. Values below gives 0 of 10 range kind of aligned
 * to non linearity of human eye
 */
typedef enum {
  LED_LVL_MIN = 0,    //!< LED_LVL_MIN
  LED_LVL_0 = 0,      //!< LED_LVL_0
  LED_LVL_1 = 0x01,   //!< LED_LVL_1
  LED_LVL_2 = 0x02,   //!< LED_LVL_2
  LED_LVL_3 = 0x04,   //!< LED_LVL_3
  LED_LVL_4 = 0x08,   //!< LED_LVL_4
  LED_LVL_5 = 0x10,   //!< LED_LVL_5
  LED_LVL_6 = 0x20,   //!< LED_LVL_6
  LED_LVL_7 = 0x40,   //!< LED_LVL_7
  LED_LVL_8 = 0x80,   //!< LED_LVL_8
  LED_LVL_9 = 0xFF,   //!< LED_LVL_9
  LED_LVL_MAX = 0xFF  //!< LED_LVL_MAX
} teLedRGBlevel;

/**
 * @brief LED color as R,G,B PWM values under one hood
 */
typedef struct {
  uint8_t u8pwmR, u8pwmG, u8pwmB;
} teLedRGBchannels;
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =========================| High level functions |==========================
/**
 * @brief Start task which handle RGB LED
 * @param pvParameters Pointer to parameters - can be empty
 */
void ledRGBtask(void *pvParameters);

/**
 * @brief Set given RGB color
 *
 * @param sColor Color as PWM values
 */
void ledRGBset(teLedRGBchannels sColor);

/**
 * @brief Get current color
 * @param[out] psColor Pointer to color structure. Color will be written here
 */
void ledRGBget(teLedRGBchannels *psColor);

/**
 * @brief Send request to task to gracefully exit
 *
 * This will just send request. It is up to RTOS and priorities to let buzzer
 * task to do it's job.
 */
void ledRGBtaskRequestExit(void);

/**
 * @brief Allows to check whether task is still running or not
 * @return True if still running, false otherwise.
 */
bool ledRGBtaskIsRunning(void);
// ========================| Middle level functions |=========================
/**
 * @brief Turn on R, G and B LED
 */
void ledTurnOnAll(void);

/**
 * @brief Turn off R, G and B LED
 */
void ledTurnOffAll(void);

/**
 * @brief Set same level on all R, G and B LED
 * @param u8pwmValue PWM value. Range 0 (off) ~ 255 (on)
 */
void ledSetAll(const uint8_t u8pwmValue);

/**
 * @brief Set all LED to same level
 * @param eLedLevel LED level as enumeration value
 */
void ledSetAllLvl(const teLedRGBlevel eLedLevel);

/**
 * @brief Increase all LED brightness level
 *
 * Using 1/10 steps. Refer to teLedRGBlevel
 */
void ledIncLvlAll(void);

/**
 * @brief Decrease all LED brightness level
 *
 * Using 1/10 steps. Refer to teLedRGBlevel
 */
void ledDecLvlAll(void);

/**
 * @brief Get LED level for all channels as average
 * @return LED level
 */
teLedRGBlevel ledGetLvlAll(void);

/**
 * @brief Increase given level
 * @param ePwmLvl Current LED brightness level
 * @return Updated LED brightness level
 */
teLedRGBlevel ledIncPwmLvl(const teLedRGBlevel ePwmLvl);
/**
 * @brief Decrease given level
 * @param ePwmLvl Current LED brightness level
 * @return Updated LED brightness level
 */
teLedRGBlevel ledDecPwmLvl(const teLedRGBlevel ePwmLvl);
// ==========================| Low level functions |==========================
/**
 * @brief Set red LED PWM value
 * @param u8pwmValue Brightness level. Range 0 (off) ~ 255 (on)
 */
void ledRGBsetR(const uint8_t u8pwmValue);

/**
 * @brief Read PWM value from PWM controller for red LED
 * @return PWM value for red LED
 */
uint8_t ledRGBgetR(void);

/**
 * @brief Set red LED level as 0/10 value
 * @param eLedLevel LED level as enumeration value
 */
void ledRGBsetRlvl(const teLedRGBlevel eLedLevel);

/**
 * @brief Get red LED level
 * @return LED level as 0/10 enum
 */
teLedRGBlevel ledRGBgetRlvl(void);

/**
 * @brief Set green LED as 0/10 value
 * @param u8pwmValue Brightness level. Range 0 (off) ~ 255 (on)
 */
void ledRGBsetG(const uint8_t u8pwmValue);

/**
 * @brief Read PWM value from PWM controller for green LED
 * @return PWM value for green LED
 */
uint8_t ledRGBgetG(void);

/**
 * @brief Set green LED level
 * @param eLedLevel LED level as enumeration value
 */
void ledRGBsetGlvl(const teLedRGBlevel eLedLevel);
/**
 * @brief Get green LED level
 * @return LED level as 0/10 enum
 */
teLedRGBlevel ledRGBgetGlvl(void);

/**
 * @brief Set blue LED as 0/10 value
 * @param u8pwmValue Brightness level. Range 0 (off) ~ 255 (on)
 */
void ledRGBsetB(const uint8_t u8pwmValue);

/**
 * @brief Read PWM value from PWM controller for blue LED
 * @return PWM value for blue LED
 */
uint8_t ledRGBgetB(void);

/**
 * @brief Set blue LED level
 * @param eLedLevel LED level as enumeration value
 */
void ledRGBsetBlvl(const teLedRGBlevel eLedLevel);

/**
 * @brief Get blue LED level
 * @return LED level as 0/10 enum
 */
teLedRGBlevel ledRGBgetBlvl(void);

#endif  // __LED_RGB_H__

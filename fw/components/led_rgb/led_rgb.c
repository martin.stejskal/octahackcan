/**
 * @file
 * @author Martin Stejskal
 * @brief Driver for RGB LED
 */
// ===============================| Includes |================================
// Standard
#include <assert.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

// ESP specific libraries
#include <driver/gpio.h>
#include <driver/ledc.h>
#include <esp_err.h>
#include <esp_log.h>

// Project specific
#include "cfg.h"
#include "led_rgb.h"
// ================================| Defines |================================
/**
 * @brief Low or high speed of whole PWM module
 *
 * Typically when high frequencies are required, high speed mode is preferred
 */
#define LED_RGB_LEDC_SPEED (LEDC_LOW_SPEED_MODE)

/**
 * @brief Total duration of whole initial animation
 *
 * Zero value disable initial animation
 */
#define INIT_ANIM_DURATION_MS (0)

/**
 * @brief Define delay in ms used in initial animation
 */
#define INIT_ANIM_DELAY_MS (20)
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
typedef enum {
  MODE_STATIC,  /// Static level - no PWM pulses at all
  MODE_PWM,     /// PWM pulses on line
} teLedMode;

typedef enum {
  NOTIFICATION_GENERIC,
  NITIFICATION_EXIT,
} teNotifyEventType;

typedef struct {
  teLedMode eLedModeRed, eLedModeGreen, eLedModeBlue;
  // Because ledc_get_duty() returns something different that what was set,
  // easy way is to store PWM values as another variables separately
  uint8_t u8pwmRed, u8pwmGreen, u8pwmBlue;

  // Handler to buzzer task. Need for effective handling requests
  TaskHandle_t xTaskHandler;

  // Says if RTOS task is active or not
  bool bIsTaskRunnig;
} tsRuntime;
// ===========================| Global variables |============================
/**
 * @brief Tag for logger
 */
static const char *TAG = "LED RGB";

/* Setup timer. Here are few catches, so let's mention few of these:
 *  * Higher PWM frequency, faster fade effect have to be. Or it have to
 *    be implemented somehow here
 *  * Higher bit resolution -> slower PWM
 *  * The ledc_set_fade_with_time() still have some bugs, so let's not
 *    use it although it seems to be nice
 */
static const ledc_timer_config_t msLedcTimerCfg = {
    // resolution of PWM duty cycle
    .duty_resolution = LEDC_TIMER_8_BIT,
    // frequency of PWM signal
    .freq_hz = 100000,
    // timer mode
    .speed_mode = LED_RGB_LEDC_SPEED,
    // timer index
    .timer_num = LED_RGB_TIMER,
    // Auto select the source clock
    .clk_cfg = LEDC_AUTO_CLK,
};

/*
 * Prepare configuration channel of LED Controller by selecting:
 * - controller's channel number
 * - output duty cycle, set initially to 0 (off)
 * - GPIO number where signal will be distributed
 * - speed mode, either high or low
 * - timer servicing selected channel
 *   Note: if different channels use one timer,
 *         then frequency and bit_num of these channels
 *         will be the same
 */
static ledc_channel_config_t msLedcChannelCfg = {
    .channel = -1,
    .duty = 0,
    .gpio_num = -1,
    .speed_mode = LED_RGB_LEDC_SPEED,
    .hpoint = 0,
    .timer_sel = LED_RGB_TIMER};

static tsRuntime msRuntime;
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
static teLedRGBlevel _pwmToLvl(const uint8_t u8pwm);
static uint8_t _getAvgPWMvalue(void);
static void _setPwmRaw(const ledc_channel_t eChannel, teLedMode *peLedMode,
                       const uint8_t u8pwm);
static void _setLedMode(const ledc_channel_t eChannel, teLedMode eLedMode);
static gpio_num_t _getGpioNum(const ledc_channel_t eChannel);
static void _setLedPwmStruct(const ledc_channel_t eChannel);
static void _playInitAnim(void);

/**
 * @brief Common exit routine for most use cases
 */
static void _exitRoutine(void);
// =========================| High level functions |==========================
void ledRGBtask(void *pvParameters) {
  // Since this functionality is not critical, error codes are not checked
  // in order to make code easier

  msRuntime.bIsTaskRunnig = true;

  ESP_LOGI(TAG, "Initializing LED RGB module");

  ledc_timer_config(&msLedcTimerCfg);

  // Set initial modes. To trigger switch to static, need to set as current
  // PWM mode
  msRuntime.eLedModeRed = msRuntime.eLedModeGreen = msRuntime.eLedModeBlue =
      MODE_PWM;

  // Shut them all down - this will also set LED PWM value in msRuntime
  ledTurnOffAll();

  // Small animation
  _playInitAnim();

  // When notification from RTOS arrive, need to store it somewhere
  teNotifyEventType eEvent;

  // Get handler to current task. Needed for notifications (instead of using
  // delays)
  msRuntime.xTaskHandler = xTaskGetCurrentTaskHandle();

  for (;;) {
    // Wait for event - currently only 1 event is supported -> quit task
    xTaskNotifyWait(0, 0, &eEvent, portMAX_DELAY);
    ESP_LOGI(TAG, "Woken up. Event: %d", eEvent);
    if (eEvent == NITIFICATION_EXIT) {
      // Request to exit task gracefully
      _exitRoutine();
      return;
    }
    // Else it is normal event - keep going
  }

  ESP_LOGE(TAG, "LED RGB task crashed");
  _exitRoutine();
}

void ledRGBset(teLedRGBchannels sColor) {
  ledRGBsetR(sColor.u8pwmR);
  ledRGBsetG(sColor.u8pwmG);
  ledRGBsetB(sColor.u8pwmB);
}

void ledRGBget(teLedRGBchannels *psColor) {
  psColor->u8pwmR = ledRGBgetR();
  psColor->u8pwmG = ledRGBgetG();
  psColor->u8pwmB = ledRGBgetB();
}

void ledRGBtaskRequestExit(void) {
  // Setup variables
  BaseType_t xHigherPriorityTaskWoken = pdFALSE;

  // Notify request
  xTaskNotifyFromISR(msRuntime.xTaskHandler, NITIFICATION_EXIT,
                     eSetValueWithOverwrite, &xHigherPriorityTaskWoken);
}

bool ledRGBtaskIsRunning(void) { return msRuntime.bIsTaskRunnig; }
// ========================| Middle level functions |=========================
void ledTurnOnAll(void) {
  ledRGBsetR(255);
  ledRGBsetG(255);
  ledRGBsetB(255);
}

void ledTurnOffAll(void) {
  ledRGBsetR(0);
  ledRGBsetG(0);
  ledRGBsetB(0);
}

void ledSetAll(const uint8_t u8pwmValue) {
  ledRGBsetR(u8pwmValue);
  ledRGBsetG(u8pwmValue);
  ledRGBsetB(u8pwmValue);
}

void ledSetAllLvl(const teLedRGBlevel eLedLevel) {
  ledSetAll((uint8_t)eLedLevel);
}

void ledIncLvlAll(void) {
  // Get level for all LED
  teLedRGBlevel eLedLevel = ledGetLvlAll();

  // Increase value
  eLedLevel = ledIncPwmLvl(eLedLevel);

  // Set new PWM value
  ledSetAllLvl(eLedLevel);
}

void ledDecLvlAll(void) {
  // Get level for all LED
  teLedRGBlevel eLedLevel = ledGetLvlAll();

  // Decrease value
  eLedLevel = ledDecPwmLvl(eLedLevel);

  // Set new PWM value
  ledSetAllLvl(eLedLevel);
}

teLedRGBlevel ledGetLvlAll(void) { return _pwmToLvl(_getAvgPWMvalue()); }

teLedRGBlevel ledIncPwmLvl(const teLedRGBlevel ePwmLvl) {
  switch (ePwmLvl) {
    case LED_LVL_0:
      return LED_LVL_1;
    case LED_LVL_1:
      return LED_LVL_2;
    case LED_LVL_2:
      return LED_LVL_3;
    case LED_LVL_3:
      return LED_LVL_4;
    case LED_LVL_4:
      return LED_LVL_5;
    case LED_LVL_5:
      return LED_LVL_6;
    case LED_LVL_6:
      return LED_LVL_7;
    case LED_LVL_7:
      return LED_LVL_8;
    case LED_LVL_8:
      return LED_LVL_9;
    case LED_LVL_9:
      // No higher level
      return LED_LVL_9;
    default:
      // This should not happen
      assert(0);
  }

  // This should not happen, but if so
  assert(0);
  return LED_LVL_MIN;
}

teLedRGBlevel ledDecPwmLvl(const teLedRGBlevel ePwmLvl) {
  switch (ePwmLvl) {
    case LED_LVL_0:
      // Can not go lower
      return LED_LVL_0;
    case LED_LVL_1:
      return LED_LVL_0;
    case LED_LVL_2:
      return LED_LVL_1;
    case LED_LVL_3:
      return LED_LVL_2;
    case LED_LVL_4:
      return LED_LVL_3;
    case LED_LVL_5:
      return LED_LVL_4;
    case LED_LVL_6:
      return LED_LVL_5;
    case LED_LVL_7:
      return LED_LVL_6;
    case LED_LVL_8:
      return LED_LVL_7;
    case LED_LVL_9:
      return LED_LVL_8;
    default:
      // This should not happen
      assert(0);
  }

  // This should not happen, but if so
  assert(0);
  return LED_LVL_MIN;
}
// ==========================| Low level functions |==========================
void ledRGBsetR(const uint8_t u8pwmValue) {
  _setPwmRaw(LED_R_PWM_CH, &msRuntime.eLedModeRed, u8pwmValue);

  // Update PWM value in runtime variable
  msRuntime.u8pwmRed = u8pwmValue;
}

uint8_t ledRGBgetR(void) { return msRuntime.u8pwmRed; }

void ledRGBsetRlvl(const teLedRGBlevel eLedLevel) {
  ledRGBsetR((uint8_t)eLedLevel);
}

teLedRGBlevel ledRGBgetRlvl(void) { return _pwmToLvl(ledRGBgetR()); }

void ledRGBsetG(const uint8_t u8pwmValue) {
  _setPwmRaw(LED_G_PWM_CH, &msRuntime.eLedModeGreen, u8pwmValue);

  // Update PWM value in runtime variable
  msRuntime.u8pwmGreen = u8pwmValue;
}

uint8_t ledRGBgetG(void) { return msRuntime.u8pwmGreen; }

void ledRGBsetGlvl(const teLedRGBlevel eLedLevel) {
  ledRGBsetG((uint8_t)eLedLevel);
}

teLedRGBlevel ledRGBgetGlvl(void) { return _pwmToLvl(ledRGBgetG()); }

void ledRGBsetB(const uint8_t u8pwmValue) {
  _setPwmRaw(LED_B_PWM_CH, &msRuntime.eLedModeBlue, u8pwmValue);

  // Update PWM value in runtime variable
  msRuntime.u8pwmBlue = u8pwmValue;
}

uint8_t ledRGBgetB(void) { return msRuntime.u8pwmBlue; }

void ledRGBsetBlvl(const teLedRGBlevel eLedLevel) {
  ledRGBsetB((uint8_t)eLedLevel);
}

teLedRGBlevel ledRGBgetBlvl(void) { return _pwmToLvl(ledRGBgetB()); }
// ==========================| Internal functions |===========================
/**
 * @brief Convert input PWM value into "brightness level" value
 *
 * Note that if value does not exactly match value, some rounding is done.
 *
 * @param u8pwm Raw PWM value. Range 0~255
 * @return Brightness level as enumeration
 */
static teLedRGBlevel _pwmToLvl(const uint8_t u8pwm) {
  if (u8pwm <= LED_LVL_0) {
    return LED_LVL_0;
  } else if (u8pwm <= LED_LVL_1) {
    return LED_LVL_1;
  } else if (u8pwm <= LED_LVL_2) {
    return LED_LVL_2;
  } else if (u8pwm <= LED_LVL_3) {
    return LED_LVL_3;
  } else if (u8pwm <= LED_LVL_4) {
    return LED_LVL_4;
  } else if (u8pwm <= LED_LVL_5) {
    return LED_LVL_5;
  } else if (u8pwm <= LED_LVL_6) {
    return LED_LVL_6;
  } else if (u8pwm <= LED_LVL_7) {
    return LED_LVL_7;
  } else if (u8pwm <= LED_LVL_8) {
    return LED_LVL_8;
  } else {
    return LED_LVL_9;
  }
}

/**
 * @brief Returns average PWM value for all channels
 * @return PWM value for all channels as average
 */
static uint8_t _getAvgPWMvalue(void) {
  // Collect values from all channels
  uint8_t u8pwmR = ledRGBgetR(), u8pwmG = ledRGBgetG(), u8pwmB = ledRGBgetB();

  uint16_t u16pwmAverage =
      ((uint16_t)u8pwmR + (uint16_t)u8pwmG + (uint16_t)u8pwmB) / 3;
  // Expect 8 bit long at top
  assert(u16pwmAverage < 256);

  return ((uint8_t)u16pwmAverage);
}

/**
 * @brief Set PWM value for given channel
 * @param eChannel PWM channel. Basically it define LED color
 * @param peLedMode Pointer to LED's mode (current)
 * @param u8pwm PWM value itself
 */
static void _setPwmRaw(const ledc_channel_t eChannel, teLedMode *peLedMode,
                       const uint8_t u8pwm) {
  assert(peLedMode);
  assert(eChannel < LEDC_CHANNEL_MAX);

  // Check for extremes. In these extremes PWM can not be used, since for
  // example 0xFF will not produce steady signal. And even very short bursts
  // can light on LED while human eye can spot it -> in these cases classic
  // digital logic will be used - only 0 and 1
  if ((u8pwm == 0) || (u8pwm == 0xFF)) {
    // Static mode - check if coming from different mode -> reconfigure
    // GPIO is required
    if (*peLedMode != MODE_STATIC) {
      // Modify mode
      *peLedMode = MODE_STATIC;
      _setLedMode(eChannel, *peLedMode);
    }

    // GPIO should be ready - just set it
    if (u8pwm == 0) {
      gpio_set_level(_getGpioNum(eChannel), 1);
    } else if (u8pwm == 0xFF) {
      gpio_set_level(_getGpioNum(eChannel), 0);
    } else {
      assert(0);
      ESP_LOGE(TAG, "Internal error. Unexpected PWM value: %u (0 or 0xFF)",
               u8pwm);
    }
  } else {
    // PWM mode
    // Reconfigure GPIO if required
    if (*peLedMode != MODE_PWM) {
      // Modify mode
      *peLedMode = MODE_PWM;
      _setLedMode(eChannel, *peLedMode);
    }

    // Physically RGB LED is enabled when output is low -> invert value
    ledc_set_duty(msLedcChannelCfg.speed_mode, eChannel, 0xFF - u8pwm);
    ledc_update_duty(msLedcChannelCfg.speed_mode, eChannel);
  }
}

/**
 * @brief Set mode for given LED
 * @param eChannel PWM channel as reference for LED color
 * @param eLedMode Required mode
 */
static void _setLedMode(const ledc_channel_t eChannel, teLedMode eLedMode) {
  // If mode "static" is used, this variable will be filled
  gpio_num_t eGpioNum;

  switch (eLedMode) {
    case MODE_STATIC:
      eGpioNum = _getGpioNum(eChannel);

      gpio_pad_select_gpio(eGpioNum);
      gpio_set_direction(eGpioNum, GPIO_MODE_OUTPUT);
      break;
    case MODE_PWM:
      _setLedPwmStruct(eChannel);
      ledc_channel_config(&msLedcChannelCfg);
      break;
  }
}

/**
 * @brief Returns GPIO number based on PWM channel
 *
 * PWM channel number basically refer to LED color which can be assigned to
 * GPIO number
 *
 * @param eChannel PWM channel for given LED
 * @return GPIO number for given PWM channel (LED color)
 */
inline static gpio_num_t _getGpioNum(const ledc_channel_t eChannel) {
  switch (eChannel) {
    case LED_R_PWM_CH:
      return LED_R_PIN;
      break;
    case LED_G_PWM_CH:
      return LED_G_PIN;
      break;
    case LED_B_PWM_CH:
      return LED_B_PIN;
      break;
    default:
      ESP_LOGE(TAG, "Unexpected LED channel: %u", eChannel);
      return GPIO_NUM_NC;
  }
}

/**
 * @brief Set part of PWM structure for given LED
 *
 * PWM setting structure is shared across module and therefore it can be
 * easily reused. This function will set it according to given PWM channel
 * (LED color basically)
 *
 * @param eChannel PWM channel (LED color)
 */
inline static void _setLedPwmStruct(const ledc_channel_t eChannel) {
  switch (eChannel) {
    case LED_R_PWM_CH:
      msLedcChannelCfg.channel = LED_R_PWM_CH;
      msLedcChannelCfg.gpio_num = LED_R_PIN;
      break;
    case LED_G_PWM_CH:
      msLedcChannelCfg.channel = LED_G_PWM_CH;
      msLedcChannelCfg.gpio_num = LED_G_PIN;
      break;
    case LED_B_PWM_CH:
      msLedcChannelCfg.channel = LED_B_PWM_CH;
      msLedcChannelCfg.gpio_num = LED_B_PIN;
      break;
    default:
      ESP_LOGE(TAG, "Unexpected LED channel: %u", eChannel);
  }
}

/**
 * @brief Small initial animation
 *
 * Just for fun and to verify functionality
 */
static void _playInitAnim(void) {
#if INIT_ANIM_DURATION_MS == 0
  return;
#else

  // List of pointers to functions
  void (*pafFunc[])(uint8_t u8pwmValue) = {&ledRGBsetR, &ledRGBsetG,
                                           &ledRGBsetB};

  // Go over R, G and B
  for (uint8_t u8funcIdx = 0; u8funcIdx < 3; u8funcIdx++) {
    // Assume that whole animation should not take some time.
    // (and still have some margin for final things). We have 3 colors ->
    // (total time)/3.
    // Assume that delay between changing intensity should be 20 ms ->
    // divided 20. And maximum value is 255
    for (uint16_t u16pwmVal = 0; u16pwmVal < 255;
         u16pwmVal +=
         (255 / ((INIT_ANIM_DURATION_MS / 3) / INIT_ANIM_DELAY_MS))) {
      (*pafFunc[u8funcIdx])((uint8_t)u16pwmVal);
      vTaskDelay(INIT_ANIM_DELAY_MS / portTICK_PERIOD_MS);
    }
    // Last step - set max PWM, wait and turn off
    (*pafFunc[u8funcIdx])(255);
    vTaskDelay(INIT_ANIM_DELAY_MS / portTICK_PERIOD_MS);
    (*pafFunc[u8funcIdx])(0);
  }
#endif
}

static void _exitRoutine(void) {
  // Disable GPIO. To do so, need to switch back to GPIO functionality
  gpio_pad_select_gpio(LED_R_PIN);
  gpio_pad_select_gpio(LED_G_PIN);
  gpio_pad_select_gpio(LED_B_PIN);

  gpio_config_t sCfg = {
      .pin_bit_mask = BIT64(LED_R_PIN) | BIT64(LED_G_PIN) | BIT64(LED_B_PIN),
      .mode = GPIO_MODE_DISABLE,
      .pull_up_en = false,  // Need to be disabled so they will not glow
      .pull_down_en = false,
      .intr_type = GPIO_INTR_DISABLE,
  };
  gpio_config(&sCfg);

  msRuntime.bIsTaskRunnig = false;

  // Have to be last, since it will basically terminate current task
  vTaskDelete(NULL);
}

#/bin/bash

# Get project directory, so everything can be done through absolute paths ->
# -> can call this script from anywhere
this_dir=$( dirname $0 )

# Backup actual directory
user_dir=$( pwd )

cd "$this_dir"/../


# Build project
docker run --rm \
  -v $PWD:/project -w /project espressif/idf \
  idf.py build

# Problem is that build folder now belongs to root user. Let's change it
sudo chown -R $(id -u):$(id -g) build

# Go back
cd $user_dir

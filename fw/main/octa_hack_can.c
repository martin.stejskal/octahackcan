/**
 * @file
 * @author Martin Stejskal
 * @brief Main user function - mainly for creating tasks
 */
// ===============================| Includes |================================
#include <driver/gpio.h>
#include <esp_log.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <sdkconfig.h>
#include <soc/cpu.h>

#include "app.h"
#include "buzzer.h"
#include "cfg.h"
#include "led_rgb.h"
#include "ui_service_core.h"
// ================================| Defines |================================
/**
 * @brief Tag for logger
 */
static const char *TAG = "Main";
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================

// ===========================| Global variables |============================

// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
// =========================| High level functions |==========================
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
#if SHOW_STACK_USAGE
static const char *STACK_TAG = "Stack usage";
/**
 * @brief Task which periodically print stack usage
 * @param pvParameters Parameters - not used actually
 */
void showStackUsage(void *pvParameters) {
  UBaseType_t uNumOfTasks;
  TaskStatus_t *pxTaskStatusArray;
  uint32_t u32TotalRunTime;

  for (;;) {
    vTaskDelay(5000 / portTICK_PERIOD_MS);
    ESP_LOGI(STACK_TAG, "=================================");

    // Take a snapshot of the number of tasks in case it changes while this
    // function is executing.
    uNumOfTasks = uxTaskGetNumberOfTasks();
    // Allocate a TaskStatus_t structure for each task.  An array could be
    // allocated statically at compile time.
    pxTaskStatusArray = pvPortMalloc(uNumOfTasks * sizeof(TaskStatus_t));

    // Pointer should not be empty, but just in case check it
    if (pxTaskStatusArray) {
      uxTaskGetSystemState(pxTaskStatusArray, uNumOfTasks, &u32TotalRunTime);
      for (int iTskCnt = 0; iTskCnt < uNumOfTasks; iTskCnt++) {
        ESP_LOGI(STACK_TAG, "%s | Free Bytes: %d",
                 pxTaskStatusArray[iTskCnt].pcTaskName,
                 pxTaskStatusArray[iTskCnt].usStackHighWaterMark);
      }
    }

    // The array is no longer needed, free the memory it consumes.
    vPortFree(pxTaskStatusArray);
    ESP_LOGI(STACK_TAG, "=================================");
  }
}
#endif

void app_main(void) {
  ESP_LOGI(TAG, "Alive! Creating tasks...");

  // Buzzer should be ready before other tasks will start to using it ->
  // before "application"
  xTaskCreate(buzzerTaskRTOS, "Buzzer", 2 * 1024, NULL, tskIDLE_PRIORITY, NULL);

  // RGB LED. Also should be started before other tasks
  xTaskCreate(ledRGBtask, "rgb", 2 * 1024, NULL, tskIDLE_PRIORITY, NULL);

  // Service UI. Allow to setup some fine settings
  xTaskCreate(uiServiceTaskRTOS, "Serv UI", 4 * 1024, NULL, tskIDLE_PRIORITY,
              NULL);

  // High level application task. This is basically "main()"
  xTaskCreate(appTask, "App", 3 * 1024, NULL, tskIDLE_PRIORITY, NULL);

#if SHOW_STACK_USAGE
  xTaskCreate(showStackUsage, "task usage", 2 * 1024, NULL, 4, NULL);
#endif

  ESP_LOGI(TAG, "Main tasks were created");
}

// ==========================| Internal functions |===========================
